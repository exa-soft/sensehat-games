"""
Module for the SenseHAT imports.
This makes it easier to switch between emulator and real hardware:
the other modules should import from there (and not directly from sense_hat or sense_emu!).
"""

# Use real hardware:
# from sense_hat import SenseHat, ACTION_PRESSED, ACTION_RELEASED, ACTION_HELD, \
#    DIRECTION_DOWN, DIRECTION_UP, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_MIDDLE

# Use emulator:
from sense_emu import SenseHat, ACTION_PRESSED, ACTION_RELEASED, ACTION_HELD, \
    DIRECTION_DOWN, DIRECTION_UP, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_MIDDLE
