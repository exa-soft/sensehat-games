"""
This module contains utils for background tasks (not for display) for all modules.
"""

__author__ = 'Edith Birrer'
__version__ = '1.1'


import logging
from time import sleep

from bombgame.core.common import SenseHat

logger = logging.getLogger('core.utils')
sense = SenseHat()


def sleep_but_getaccelero(seconds: float):
    """Sleep the given number of seconds, but continue getting
    accelerometer readings (so we do not have "old" values when
    we need them later)
    """
    for i in range (int(seconds * 19)):
        v = sense.get_accelerometer_raw()
        sleep(0.05)


def _test():
    sense.clear()
    sleep_but_getaccelero(1)


if __name__ == "__main__":  # to start this: `python3 -m bombgame.core.displayUtils`
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-15s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=f)
    logger.setLevel(logging.DEBUG)

    sense.low_light = False
    # uncomment one line for a test:
    _test()
