"""
This module contains the "window collection", which is an imaginary
rectangle of several "GameWindows", each with its game, so the user can
move from one to another by using the joystick. The whole can be used
to implement games like "Keep talking and nobody explodes".

Usage:

1. Change width and height of the grid, if necessary.
2. Init each place of the grid with a game (with set_game()).
3. Call start().

"""

__author__ = 'Edith Birrer'
__version__ = '1.1'


import logging
from threading import Condition

from bombgame.countdown.countdownWindow import CountdownWindow, COUNTDOWN_ID
from bombgame.countdown.displayUtils import show_exploding_bomb
from bombgame.output import displayUtils as dU
from bombgame.output import fieldScroller
from bombgame.output import pixelUtils as pU
from bombgame.output.serialNumberWindow import SERIAL_ID, SerialNumberWindow
from .common import SenseHat, ACTION_PRESSED, \
    DIRECTION_DOWN, DIRECTION_UP, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_MIDDLE
from .exceptions import ArgumentError
from .game import GameWindow

logger = logging.getLogger('core.gameWindowGrid')
sense = SenseHat()


sol = 'https://minigames.fmgsoftware.ch/nmm'


class GameWindowGrid:

    width = 3       # width of the games grid, defaults to 3
    height = 2      # height of the games grid
    _pos_x = 0        # X-position of the current game in the grid (0 is left)
    _pos_y = 0        # Y-position of the current game in the grid (0 is bottom)
    scroll_speed = 0.2      # Speed of scrolling. Number is wait time between steps (higher means slower).
    _games = None           # array of games
    _game_status_solved = None     # map of gameIds and their status (solved or not)
    _sync_cond = None       # Condition used for synchronization between processes
    _req_direction = ''     # direction where the grid should move (as soon as the game allows it)

    def __init__(self, w, h, **kwargs):
        self.width = w
        self.height = h
        # self._games = [[None for x in range(self.width)] for y in range(self.height)]
        self._games = [[GameWindow for y in range(self.height)] for x in range(self.width)]
        # TODO LATER implement setting scroll_speed
        # if not kwargs is None:
        #     if not (kwargs['scroll_speed'] is None):
        #         self.scroll_speed = kwargs['scroll_speed']
        self._sync_cond = Condition()
        self._game_status_solved = {}
        logger.info('GameWindowGrid initialized: {}x{} games'.format(self.width, self.height))

    def set_game(self, x: int, y: int, game: GameWindow):
        """
        Set a game to a position (x, y) in the game grid.
        :param x:   x position for the game (0 is left)
        :param y:   y position for the game (0 is bottom)
        :param game:    game to set
        """
        if x >= self.width:
            raise ArgumentError(x, "x must be 0 < x <= width")
        if y >= self.height:
            raise ArgumentError(y, "y must be 0 < y <= height")

        # what the grid does when the game finds that the user wants to move in the grid
        game.set_move_callback(self.execute_move)
        # what the grid does when the game informs that a game became solved
        game.set_signalsolved_cbk(self.handle_solved)
        # what the grid does when the game loop finds that time is up
        game.set_gameover_callback(self.handle_game_over)
        logger.debug('set handlers to game "{}"'.format(game))
        self._games[x][y] = game

        # if the game is the countdown window, set game_over callback
        if isinstance(game, CountdownWindow):
            # make sure the id is the standard one for countdowns
            game.game_id = COUNTDOWN_ID
            # set a special callback handler for time expiration (set from countdown Thread)
            game.set_signalexpired_cbk(self.signal_game_over)
            logger.debug('set expired handler ({1}) to game "{0}"'.format(game, self.signal_game_over))
        # if the game is the serial number, make sure the id is the standard for serial
        if isinstance(game, SerialNumberWindow):
            game.game_id = SERIAL_ID

        # insert the game into the dictionary with game status (if already solved)
        # gamid = COUNTDOWN_ID if isinstance(game, CountdownWindow) else game.game_id
        gamid = game.game_id
        logger.debug('- game id is "{}"'.format(game.game_id))
        if gamid in self._game_status_solved:
            raise ArgumentError(gamid, 'game with id "{}" already in grid'.format(gamid))
        self._game_status_solved[gamid] = False  # game not yet solved
        logger.debug('- game_solved_status: {}'.format(self._game_status_solved))

    def start(self, main_sense, pos_x=0, pos_y=0):
        """
        Start the overall game (this starts the current game).
        After all the games have been initialized, call this method to start the first game
        (the one at position pos_x, pos_y).
        :param main_sense:  the SenseHAT that will react on the joystick events (must be the "first" one)
        :param pos_x: position x where to start in the grid (default 0 = leftmost)
        :param pos_y: position y where to start in the grid (default 0 = bottom)
        """
        main_sense.stick.direction_any = self.handle_event
        self._pos_x = pos_x
        self._pos_y = pos_y
        next_game = self.get_current_game()
        next_game.resume_game()

    def get_game(self, x, y) -> GameWindow:
        """
        Return the game at the position x, y.
        :param x:   x position for the game (0 is left)
        :param y:   y position for the game (0 is bottom)
        :return:    the game at the selected position
        """
        if x >= self.width:
            raise ArgumentError(x, "x must be 0 < x <= width")
        if y >= self.height:
            raise ArgumentError(y, "y must be 0 < y <= height")
        return self._games[x][y]

    def get_current_game(self) -> GameWindow:
        """Get the current game.
        :return:    current game - the one at position (self.pos_x, self.pos_y)
        """
        return self.get_game(self._pos_x, self._pos_y)

    def handle_event(self, event):
        logger.debug("handle_event: joystick was {} {}".format(event.action, event.direction))
        if event.action == ACTION_PRESSED:
            if event.direction == DIRECTION_MIDDLE:
                logger.debug('- direction middle: ignoring event')
            else:       # other directions
                self.request_move(event.direction)

    def request_move(self, direction):
        """Ask the grid to move to the requested direction as soon as possible.

        This method is called from the thread handling joystick events.
        It sets the req_direction, this is evaluated after every game loop, so
        it will cause the grid to move when the game allows it.
        :param direction:   one of the directions of a joystick event (a string)
        """
        logger.info('move requested in direction {}'.format(direction))
        with self._sync_cond:
            if self._req_direction == '':
                self._req_direction = direction
                self.get_current_game().request_leave()
                logger.debug('- set move request to {}'.format(self._req_direction))
            else:
                logger.debug('- move request ignored (already set to {})'.format(self._req_direction))

    def execute_move(self):
        """
        Move into the requested direction in the game grid.

        This method is called from the game in the main thread (via the game loop, as
        callback method). It will reset the req_direction.
        """
        with self._sync_cond:
            direction = self._req_direction
            self._req_direction = ''
            logger.debug('- will execute move request to {}'.format(direction))

        if direction == DIRECTION_UP:
            self.go_vertical(True)
        elif direction == DIRECTION_DOWN:
            self.go_vertical(False)
        elif direction == DIRECTION_LEFT:
            self.go_horizontal(True)
        elif direction == DIRECTION_RIGHT:
            self.go_horizontal(False)
        else:
            logger.warning('- execute_move found unknown direction: {}'.format(direction))

    def go_vertical(self, go_up):
        """Move to the game above or below the current one, if possible.

        If possible, moves the current position up/down and returns True.
        Returns False if the current position is already in the topmost/bottommost row.
        :param go_up: True to scroll up, false to scroll down
        """
        pre = 'go_up' if go_up else 'go_down'
        logger.info('{} requested from current pos {}/{}'.format(pre, self._pos_x, self._pos_y))
        this_game = self.get_current_game()
        old_color = this_game.get_border_color()

        # keep in mind: to go to the game above, the screen scrolls down etc...
        this_game.leave_game()
        if go_up and self._pos_y == 0:
            logger.debug('go_up: no game above')
            fieldScroller.try_scroll_down(old_color, self.scroll_speed)
            this_game.resume_game()
        elif not go_up and self._pos_y == self.height - 1:
            logger.debug('go_down: no game below')
            fieldScroller.try_scroll_up(old_color, self.scroll_speed)
            this_game.resume_game()
        else:
            self._pos_y = self._pos_y - 1 if go_up else self._pos_y + 1
            next_game = self.get_current_game()
            logger.debug('{}: next game (at pos {}/{}): {}'
                         .format(pre, self._pos_x, self._pos_y, next_game.game_id))
            data = next_game.get_screen()
            new_color = next_game.get_border_color()
            if go_up:
                fieldScroller.scroll_down(data, [old_color, new_color], self.scroll_speed)
            else:
                fieldScroller.scroll_up(data, [old_color, new_color], self.scroll_speed)
            next_game.resume_game()

    def go_horizontal(self, go_left):
        """Move to the game left or right the current one, if possible.

        If possible, moves the current position left/right and returns True.
        Returns False if the current position is already in the leftmost/rightmost row.
        :param go_left: True to scroll left, false to scroll right
        """
        pre = 'go_left' if go_left else 'go_right'
        logger.info('{} requested from current pos {}/{}'.format(pre, self._pos_x, self._pos_y))
        this_game = self.get_current_game()
        old_color = this_game.get_border_color()

        # keep in mind: to go to the game to the left, the screen scrolls right etc...
        this_game.leave_game()
        if go_left and self._pos_x == 0:
            logger.debug('go_left: no game to he left')
            fieldScroller.try_scroll_right(old_color, self.scroll_speed)
            this_game.resume_game()
        elif not go_left and self._pos_x == self.width - 1:
            logger.debug('go_right: no game to the right')
            fieldScroller.try_scroll_left(old_color, self.scroll_speed)
            this_game.resume_game()
        else:
            self._pos_x = self._pos_x - 1 if go_left else self._pos_x + 1
            next_game = self.get_current_game()
            logger.debug('{}: next game (at pos {}/{}): {}'
                         .format(pre, self._pos_x, self._pos_y, next_game.game_id))
            data = next_game.get_screen()
            new_color = next_game.get_border_color()
            if go_left:
                fieldScroller.scroll_right(data, [old_color, new_color], self.scroll_speed)
            else:
                fieldScroller.scroll_left(data, [old_color, new_color], self.scroll_speed)
            next_game.resume_game()

    def do_with_all_games(self, func):
        """Applies func to each game. func must take the game as param."""
        for x in range(self.width):
            for y in range(self.height):
                game = self.get_game(x, y)
                if game is not None:
                    func(game)

    def all_games_solved(self):
        """Check if there is still an unsolved game, then return False.
        Otherwise return True."""
        logger.debug('check if all games solved:')
        for g, status_solved in self._game_status_solved.items():
            # do not check countdown nor serial (will never get solved)
            if g == COUNTDOWN_ID or g == SERIAL_ID:
                logger.debug('- game {} will never become solved, skip'.format(g))
            else:
                logger.debug('- game {} is solved: {}'.format(g, status_solved))
                if not status_solved:
                    return False
        return True     # all games (except countdown) have been solved

    def handle_solved(self, game_id):
        """Called in the game loop when a game became solved.
        Check if all games are solved, and if so, inform the games that
        all have been solved and display the solution.
        """
        logger.info('game {} became solved!'.format(game_id))
        if self._game_status_solved[game_id]:
            logger.warning('but game {} had already been solved before?!?!'
                           .format(game_id))
        else:
            self._game_status_solved[game_id] = True
            if self.all_games_solved():
                logger.info('all games solved ==> game over!!!')
                # after displaying solution, the game loop should end
                self.do_with_all_games(game_stop_loop)
                _show_end()

    def signal_game_over(self):
        """This is the callback function that will be called from the
        CountdownWindow when the timer is up (by the timer thread).
        Checks if all games are solved. If not,it informs all games
        that the game is over.
        """
        if self.all_games_solved():
            logger.info('time expired, but games had been solved before')
        else:
            self.do_with_all_games(game_set_game_over)
            # this causes the next invocation of the game loop to come
            # back to the grid and execute handle_game_over()

    def handle_game_over(self):
        """This is the callback function that will be called in the game
        when it finds that the time is up (called in the game loop when
        game.game_over)."""
        logger.info('=== game over ===')
        self.do_with_all_games(game_stop_loop)
        show_exploding_bomb()
        # then control goes back to the game loop, which will stop because of the flag

    def __str__(self):
        return 'GameWindowGrid with {}x{} games, position {}/{}' \
            .format(self.width, self.height, self._pos_x, self._pos_y)


def _show_end():
    """
    Displays growing green circles, to mark the game's end
    """
    dU.display_allsolved(sense)
    s = ''
    # TODO change repetition number
    for i in range(4, -1, -1):
        m = s.join([chr(c) for c in pU.testPix2]).format(s.join([chr(p) for p in pU.testPix1]), i)
        sense.show_message(m, scroll_speed=0.08, text_colour=[248, 144, 0], back_colour=[0, 80, 0])


def game_set_game_over(game):
    """to be used with to_all_games"""
    logger.debug('- set to game "{}" that game is over'.format(game.game_id))
    game.game_over = True


def game_stop_loop(game):
    logger.debug('- set to game "{}" to stop loop'.format(game.game_id))
    game.stop_loop = True
