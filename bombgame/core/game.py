"""
This module contains the core of this collection of SenseHAT games:
This class "GameWindow" implements a "window" (containing one game
that runs on the SenseHAT screen). It is used in combination with the
GameWindowGrid, a "window collection", which is an imaginary rectangle
of several "windows", each with its game, so the user can move from
one to another by using the joystick. The whole can be used to
implement games like "Keep talking and nobody explodes".
"""

__author__ = 'Edith Birrer'
__version__ = '1.0'

import logging

from bombgame.core.utils import sleep_but_getaccelero
from .common import SenseHat
from time import sleep

logger = logging.getLogger('core.game')

sense = SenseHat()


# noinspection PyPep8Naming
class GameWindow:
    """Base class for games working on a SenseHAT screen.
    This is the base class to implement games that are played in a 
    "window" of one SenseHAT screen and can be combined with other such
    games.
    
    Subclasses must overwrite:
    - init_game()  - will be called by __init__()
    - get_name()
    - start_game()
    - play()
    - is_solved()

    Optionally, subclasses can overwrite:
    - pause_game()
    - continue_game()
    - get_screen()
    - get_solved_screen()
    - get_border_color()

    Note that a game in GameWindow cannot use the joystick events 
    up/down/left/right, because they are already used to "scroll" from 
    one GameWindow ot its neighbours. However, a game in GameWindow
    could use the middle button of the joystick (not yet implemented).
    """

    def __init__(self, gameId, **moreArgs):
        """Init the game window class.

        Implementations and classes using GameWindows should provide
        suitable gameIds (the gameId is only used to distinguish the games
        in a GameWindowGrid, but then, it is important and must be unique).

        :param gameId:  an ID of the game window (no check is made if unique).
        :param move_callback:   the function to be executed to execute the move in the grid
        :param solved_callback: the function to signal to the grid that the game is solved
        """

        logger.debug('in __init__:  gameId {}, moreArgs {}'.format(gameId, moreArgs))
        self.game_id = gameId
        self.started = False
        self.running = False
        self.leave_req = False
        self.stop_loop = False      # the main game loop will only end when this is true
        self.game_over = False          # will only be set by GameWindowGrid
        self.saved_screen = None

        self._move_callback = None      # will be set when game is inserted into grid
        self._signalsolved_cbk = None   # will be set when game is inserted into grid
        self._gameover_callback = None  # will be set when game is inserted into grid
        self.init_game(**moreArgs)

    def init_game(self, **moreArgs):
        """Init game.

        Initialize the game logic.   
        Subclasses should overwrite this method - it will be called from
        __init__ with the parameters in moreArgs.
        """
        logger.debug('init game {} - have moreArgs {}'.format(self.game_id, moreArgs))
        # should be overwritten by subclasses 

    def set_move_callback(self, move_function):
        """
        Set the function for handling moves in the game grid.
        This will be set when the game is inserted into the grid.
        :param move_function:  the function used for moving within the game grid
        """
        self._move_callback = move_function

    def set_signalsolved_cbk(self, solved_function):
        """
        Set the function for signalling to the game grid that the game was solved.
        This will be set when the game is inserted into the grid.
        The function must take 1 argument (the game id).
        :param solved_function:  the function to call when the game becomes solved (1 arg)
        """
        self._signalsolved_cbk = solved_function

    def set_gameover_callback(self, gameover_function):
        """
        Set the function that will be called when the game loop detects
        self._game_over.
        This will be set when the game is inserted into the grid.
        :param gameover_function:  the function to call when time in the game grid has expired
        """
        self._gameover_callback = gameover_function

    def get_name(self):
        """ Returns the name of the game.
        
        Return the name of the game.  This may make usage of the game
        ID or just return the general name of this game.  
        Subclasses should overwrite this method.
        """
        return "(no name set - overwrite method 'get_name')"
        # should be overwritten by subclasses 

    def get_screen(self):
        """Returns the stored screen. 
        
        Get the screen for restoring the game (a 64-element array of 
        color tuples).  The default implementation returns the screen 
        stored in leave_game(), and a black screen for the very first 
        call of this game window.  For other displays on restore, 
        subclasses can overwrite this method.
        """
        return self.saved_screen if self.saved_screen is not None \
            else [(0, 0, 0)] * 64
        # can be overwritten by subclasses 

    def get_border_color(self):
        """Color for the border.  
        
        Color for the border (used when scrolling), can serve to 
        easier identify the game.  
        Subclasses should overwrite this method.
        """
        return [180, 180, 180]
        # should be overwritten by subclasses 

    def resume_game(self):
        """Starts or continues playing the game.
        
        Starts or continues playing the game (after calling
        get_screen and scrolling to the game window):
        If it has not yet been started, start_game() is called.
        Otherwise, if it is not yet solved, continue_game() is called.
        Otherwise, nothing happens.
        """
        if self.started:
            logger.info('resume_game {}: continue game'
                        .format(self.game_id))
            self.continue_game()
        else:
            logger.info('resume_game {}: start game'
                        .format(self.game_id))
            self.started = True
            self.start_game()

        logger.info('resume_game {}: calling play()'
                    .format(self.game_id))
        self.running = True
        self._play_loop()

    def start_game(self):
        """Start the game (for the first time).

        Start the game.  Will be called when resume_game() is 
        called for the first time. 
        Subclasses should overwrite this method.
        Just prepare here, this is not the game loop!
        """
        pass
        # should be overwritten by subclasses 

    def continue_game(self):
        """Continue the game: restore after pausing.

        Continue the game. Will be called when resume_game() is 
        called not for the first time, but the game is not yet solved.
        Subclassed can do things that are necessary when resuming a 
        game from the hidden status. Afterwards, play() will be called,
        so continue_game() must not call play(). Also, when 
        continue_game() is called, restoring the screen has already 
        been done as part of the scrolling back to the game.
        """
        pass
        # can be overwritten by subclasses 

    def pause_game(self):
        """Hook to implement operations before leaving the game.

        If necessary, the subclass can do here some saving or 
        cleanup before leaving the game. Will be called in leave_game(). 
        However, saving and restoring the current screen is already 
        taken care of (will be done in leave_game() and get_screen(), 
        as part of the scrolling operation).
        """
        pass
        # can be overwritten by subclasses 

    def request_leave(self):
        """Ask a game to prepare as soon as possible for being paused.

        This method is called from the thread handling joystick events (via
        the game grid): sets a flag that is evaluated after every game loop
        (this will call the callback function of the grid when found true).
        """
        logger.info('leave requested for game {}'.format(self.game_id))
        # no need to get a condition, because when this method is executed,
        # we already have the sync_cond from the grid
        if not self.leave_req:
            self.leave_req = True
            logger.debug('- set leave_req to TRUE for game {}'.format(self.game_id))

    def leave_game(self):
        """Leave the game (will save screen and call pause_game()).

        This method is called from the main thread executing the games.
        Before leaving the game, the screen will be saved. 
        Subclasses should overwrite pause_game() if they need to do
        some saving or cleanup before leaving the game. (The screen
        will be saved after pause_game().)"""
        logger.info('leave game {}'.format(self.game_id))
        self.running = False
        self.pause_game()
        self._save_screen()
        self.leave_req = False
        logger.debug('- set leave requested to FALSE for game {}'.format(self.game_id))

    def _save_screen(self):
        """Save the screen before leaving the game."""
        self.saved_screen = sense.get_pixels()
        # logger.debug('saved screen (len: {}): {}'.format(len(self.savedScreen), self.savedScreen))

    def is_solved(self):
        """Return true if the game is solved.

        Subclasses should overwrite this method. The status must
        only change during the execution of play()!
        """
        return False
        # must be overwritten by subclasses

    def get_solved_screen(self):
        """
        Return a default screen to display when the game is solved.
        Subclasses can overwrite this method
        :return:    data for a screen (64 color tuples)
        """
        a = (248, 248, 248)
        b = (192, 248, 192)
        c = (0, 248, 0)
        d = (0, 148, 0)
        e = (0, 0, 0)
        return [
            e, d, d, d, d, d, d, e,
            d, d, c, c, c, c, d, d,
            d, c, c, b, b, c, c, d,
            d, c, b, a, a, b, c, d,
            d, c, b, a, a, b, c, d,
            d, c, c, b, b, c, c, d,
            d, d, c, c, c, c, d, d,
            e, d, d, d, d, d, d, e
        ]

    def _play_loop(self):
        """The game loop.
        Checks if leaving the game has been requested, otherwise play() is called.
        """
        while not self.stop_loop:
            if self.game_over:
                logger.debug('playloop in {}: found that time expired'.format(self.game_id))
                # TODO maybe remove this
                sleep_but_getaccelero(1)
                # check if we have a handler (so games can be played without a grid)
                if self._gameover_callback is not None:
                    self._gameover_callback()
            elif self.leave_req:
                logger.debug('playloop in {}: found that leave requested'.format(self.game_id))
                self._move_callback()
                break
            elif self.is_solved():
                # used when returning to a solved game before all are solved
                logger.debug('playloop in {}: found that game already was solved'.format(self.game_id))
                self.show_solved_anim()
            else:   # not solved, not time expired -> just play one more loop
                self.play()
                if self.is_solved():
                    logger.info('playloop in {}: ------->>> game was solved just now! <<<------- '
                                .format(self.game_id))
                    # check if we have a handler (so games can be played without a grid)
                    if self._signalsolved_cbk is not None:
                        self._signalsolved_cbk(self.game_id)     # signal solved to the grid

    def play(self):
        """Play the game: main game loop.

        Subclasses must overwrite this method.
        Here the game will run, i.e. check for user input via the sensors
        and react on it. This method will be called in a loop! So subclasses
        must not implement their own game loop, but must implement only one
        "turn" in their play() implementation. If the game is solved,
        show_solved_anim() is displayed instead (even after switching games).
        Games must not use joystick events (they are used for switching between
        games (maybe the middle one can be used later).
        TODO check if we need middle event (not yet implemented and would make things complicated)
        If necessary, subclasses can check self.isRunning to see whether
        the game is still on screen. Changing between games is done 
        through resume_game() and leave_game(). Subclasses can overwrite
        continue_game() and pause_game() to implement what is necessary 
        for their game to be switched.
        """
        sense.show_letter(self.game_id[0], self.get_border_color())
        sleep(2)
        logger.info('no game implemented! Overwrite play(). Game is {}'. format(self))

    def show_solved_anim(self):
        """Show the solved animation.

        Subclasses can overwrite this method.
        Works similar to play(), but is called when the game is already solved.
        This method will be called in a loop! So subclasses must not implement
        their own loop, but must implement only one "turn" in their implementation.
        The default implementation displays a green circle for 1 second..
        """
        sense.set_pixels(self.get_solved_screen())
        sleep(1)

    def set_game_over(self):
        """Is tested in the main game loop, if found true,
        the game ends immediately."""
        self._game_over = True

    def __str__(self):
        return 'Game "{sf.game_id}", running={sf.running}, leave_req={sf.leave_req}'.format(sf=self)


def _dummy_move():
    logger.info('dummy_move - no grid in this test')


def _dummy_solved():
    logger.info('dummy_solved - GAME WAS SOLVED!')


def _test():
    import time
    import threading

    sense.low_light = False

    # global gm
    gm = GameWindow('BaseGame')
    gm.set_move_callback(_dummy_move)
    gm.set_signalsolved_cbk(_dummy_solved)
    logger.info('TEST 1: game is {}'.format(gm))
    logger.info('{} threads: {}'.format(threading.activeCount(), threading.enumerate()))

    # start the game in a separate thread
    t = threading.Thread(target=gm.resume_game)
    t.start()
    logger.info('{} threads: {}'.format(threading.activeCount(), threading.enumerate()))
    logger.info('TEST 2: game is {}\n'.format(gm))
    time.sleep(5)

    gm.request_leave()
    pixlist = [(120, 160, 80)] * 64
    sense.set_pixels(pixlist)
    logger.info('TEST 3: before leave_game: {}\n'.format(gm))
    time.sleep(1)
    gm.leave_game()     # no grid, so we call it ourselves
    logger.info('TEST 4: after leave_game: {}\n'.format(gm))
    time.sleep(5)

    logger.info('{} threads: {}'.format(threading.activeCount(), threading.enumerate()))
    t = threading.Thread(target=gm.resume_game)
    t.start()
    logger.info('{} threads: {}'.format(threading.activeCount(), threading.enumerate()))
    logger.info('TEST 5: resumed: {}\n'.format(gm))
    time.sleep(3)

    gm.request_leave()
    logger.info('{} threads: {}'.format(threading.activeCount(), threading.enumerate()))
    logger.info('This is a very basic test. For better tests of this '
                + 'class, run counter._test()')


if __name__ == "__main__":  # to start this: `python3 -m bombgame.core.game`
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-10s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=f)
    logger.setLevel(logging.DEBUG)
    # create console handler (could change log level)
    # ch = logging.StreamHandler()
    # ch.setLevel(logging.ERROR)
    # logger.addHandler(ch)
    _test()
