"""
This file contains the code to start (and maybe restart) a game in a
GameWindowGrid. After the end of the game, it asks if to restart the
game or if to shutdown the system.

Usage:

1. Set  which games are in the grid: ....
2. Start the game: ... - call start().
3. ...

"""

__author__ = 'Edith Birrer'
__version__ = '2.0'


import logging
from enum import IntEnum, unique
from signal import signal, SIGINT, SIGTERM
from time import sleep

from bombgame.core.common import SenseHat, ACTION_PRESSED, DIRECTION_MIDDLE
from bombgame.core.gameWindowGrid import GameWindowGrid
from bombgame.countdown.countdownWindow import CountdownWindow
# from dummy.dummyGame import DummyGame
from bombgame.output.serialNumberWindow import SerialNumberWindow
from bombgame.simon.displayUtils import ColorPair
from bombgame.simon.simonOIWindow import SimonOIWindow
from bombgame.simplewires.wiresOIWindow import WiresOIWindow
from bombgame.simplewires.wiresSolution import random_serial

SOLUTION = "edith-birrer.ch/losung"
# SOLUTION = "fmg-software.ch/gummi"

@unique
class Nextaction(IntEnum):
    UNDEFINED = -1      # "nothing"
    RESTART = 0         # restart the game
    SHUTDOWN = 1        # shutdown the RasPi


restart_color = (0, 80, 248)
shutdown_color = (248, 120, 120)

logger = logging.getLogger('gameStarter')
sense = SenseHat()


_listen_middlestick = False
_got_button_press = False
global _next_action
_next_action = Nextaction.RESTART     # to start the loop the first time


def finish():
    sense.clear()
    raise SystemExit(0)


def sigint(signum, frame):
    """Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    logger.warning('user pressed Ctrl-C')
    finish()


def sigterm(signum, frame):
    """Handler to finish the program upon SIGTERM event.
    To use this, register the event and this function in the main program:
    signal(SIGTERM, sigterm)
    """
    logger.warning('got sigterm signal')
    finish()


def handle_event(event):
    """
    This handler will only do something if _listen_middlestick is True.
    It will set _got_button_press to True if the joystick button is
    pressed. In addition, if _next_action == Nextaction.SHUTDOWN,
    it will immediately call shutdown().
    """
    # logger.debug("handle_event: joystick was {} {}".format(event.action, event.direction))
    if not _listen_middlestick:
        logger.debug('have joystick event, but currently not listening')
    elif event.action == ACTION_PRESSED and event.direction == DIRECTION_MIDDLE:
        logger.debug('have middle joystick pressed event')
        global _got_button_press
        _got_button_press = True
        # if _next_action == Nextaction.SHUTDOWN:
        #     logger.info('next action is shutdown - do it later')
        # elif _next_action == Nextaction.RESTART:
        #     logger.info('next action is restart - do it later')
        # else:
        #     logger.debug('next action is nothing')
    else:  # other directions or other events - ignore silently
        pass


def _ask_and_wait(msg: str, colors: ColorPair, seconds: int) -> bool:
    """Return True as soon as user presses joystick, or False after
    the given number of seconds.
    :param msg:     message to display on SenseHAT
    :param colors:  pair of foreground and background color
    :param seconds: how many seconds to wait
    """
    global _got_button_press
    global _listen_middlestick
    text_col = colors[0]
    bg_col = colors[1]
    sense.show_message(msg, scroll_speed=0.08, text_colour=text_col, back_colour=bg_col)
    sense.show_letter('?', text_colour=text_col, back_colour=bg_col)
    _got_button_press = False       # will be set from the thread that handles events
    _listen_middlestick = True
    # poll for about the given number of seconds whether user presses joystick
    for i in range(seconds * 10):
        sleep(0.1)
        if _got_button_press:
            logger.debug('got button press (in loop {})'.format(i))
            return True
    _listen_middlestick = False
    return False


def _ask_restart(seconds: int) -> bool:
    global _next_action
    _next_action = Nextaction.RESTART
    msg = 'New game?'
    colors = (restart_color, (0, 0, 0))
    return _ask_and_wait(msg, colors, seconds)


def _ask_shutdown(seconds: int):
    """Asks the user if to shut down. If the user presses the
    button, this will be done immediately (then this method
    does not return)."""
    global _next_action
    _next_action = Nextaction.SHUTDOWN
    msg = 'Shutdown? (press button)'
    colors = (shutdown_color, (0, 0, 0))
    return _ask_and_wait(msg, colors, seconds)


def shutdown():
    logger.debug('SHUTDOWN - not yet implemented. Just finish Python program')
    finish()
    # TODO shutdown raspi - is this possible?


def run_game() -> bool:
    """
    Start a game round and return the result: True if solved, False if the bomb has exploded.

    """
    grid = GameWindowGrid(2, 2)
    serial_no = random_serial(8)

    wires = WiresOIWindow('Wires1', serial = serial_no)
    simon = SimonOIWindow('Simon', len=2)
    count = CountdownWindow('countdown', duration=120)
    serial = SerialNumberWindow('Serial', serial = serial_no)
    logging.debug(grid._games)

    grid.set_game(0, 0, serial)
    grid.set_game(1, 0, simon)
    grid.set_game(0, 1, count)
    grid.set_game(1, 1, wires)
    grid.start(sense, 0, 1) # start at countdown window

    return grid.all_games_solved()


def run_dummy_game() -> bool:
    # "pseudo-game" to test the asking of the user without
    # having to play the game...
    sense.show_letter('G')
    sleep(3)
    return True


if __name__ == "__main__":      # to start this: `python3 -m gameStarter` (or `python3 gameStarter.py`)
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.INFO, format=f)
    # logging.getLogger('core.game').setLevel(logging.INFO)
    # logging.getLogger('core.gameWindowGrid').setLevel(logging.DEBUG)
    logger.setLevel(logging.DEBUG)

    # Set up signal handler to catch Ctrl-c
    signal(SIGINT, sigint)
    # set up signal handler to catch sigterm (sent by systemd)
    signal(SIGTERM, sigterm)

    sense.stick.direction_middle = handle_event
    logger.debug('handler for middle stick registered')
    sense.low_light = True
    # sense.low_light = False

    while _next_action == Nextaction.RESTART:
        _next_action = Nextaction.UNDEFINED

        game_solved = run_game()
        # game_solved = run_dummy_game()      # for testing
        logger.info('all_games_solved was {}'.format(game_solved))

        ask_repetitions = 10            # set how many times we ask before stopping automatically
        for i in range(ask_repetitions, 0, -1):
            logger.debug('will ask user {} more times if restart or shutdown'
                         .format(i))
            yes = _ask_restart(3)       # this sets _next_action to Nextaction.RESTART
            if yes:
                sense.clear(restart_color)
                sleep(0.5)
                break   # leave the for loop - continue with next interation of while loop
            yes = _ask_shutdown(3)      # this sets _next_action to Nextaction.SHUTDOWN
            if yes:
                shutdown()

        # if we arrive here, _next_action is Nextaction.SHUTDOWN, so the while loop will end

    # after asking so many times, shutdown anyway
    logger.debug('no user action for a long time -> shutdown')
    shutdown()

    # to start this: `python3 -m bombgame.gameStarter` (or `python3 gameStarter.py`)
