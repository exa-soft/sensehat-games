"""
This module contains utils for drawing the bomb, the fuse etc.
It also has a method that shows an animation of an "explosion"
on the SenseHAT screen.
The method with names "draw" do not include waiting, the methods with names
"show" (or "blink") include waiting (usually with getting accelerometer values).
"""

__author__ = 'Edith Birrer'
__version__ = '1.0'


import random
from time import sleep
from colorsys import hsv_to_rgb
from bombgame.core.common import SenseHat
from bombgame.core.utils import sleep_but_getaccelero
from bombgame.core.exceptions import DataError
import logging


logger = logging.getLogger('countdown.utils')
sense = SenseHat()


black = 0, 0, 0
bg_color = 108, 108, 108
bomb_color1 = 0, 0, 64
bomb_color2 = 32, 32, 248
bomb_burnt_color = 248, 40, 40
cord_color = 80, 0, 80
bg_color_exploded = (80, 0, 0)
fire_colors = (
    (248, 40, 0), (248, 80, 0), (248, 120, 0), (248, 160, 0), (248, 200, 0), (248, 240, 0),
    (248, 0, 0), (248, 40, 0), (248, 80, 0), (248, 120, 0), (248, 160, 0), (248, 200, 0),
    (200, 0, 0), (200, 40, 0), (200, 80, 0),
    (248, 40, 40), (248, 120, 40), (248, 200, 40),
    (248, 240, 80),
    (248, 240, 120),
    (248, 240, 160), (248, 240, 200)
)

bomb_pixels = ((x, y) for x in range(5) for y in range(5)\
               if not (x in (0, 4) and y in (0, 4)))
bomb_fuse_pixel = (5, 2)    # where the fuse joins the bomb

# array containing all bomb pixels (used for choice in explosion)
bomb_pixel_array = []
for p in bomb_pixels:
    bomb_pixel_array.append(p)
bomb_pixel_array.append(bomb_fuse_pixel)

fuse_pixels = (
    (6, 2), (7, 3), (6, 4), (6, 5), (7, 6),
    (6, 7), (5, 7), (4, 6), (3, 7), (2, 7)
)
full_fuse_len = len(fuse_pixels)  # should be 10
if full_fuse_len != 10:
    raise DataError(full_fuse_len, 'fuse length is not 10')


def scale_tuple(tup):
    return tuple(int(itup * 255) for itup in tup)


def get_fusepixel_color(fractal_left):
    """
    Get the color for the end of the fuse (depends on how much time
    is left for that fuse pixel)
    :param fractal_left:    a number between 0 and 1
    """
    # for the color, use the fractal part of the counter as hue,
    # multiplied by 0.6 (so it starts at blue and goes down to red)
    fractals = (fractal_left % 1.0)     # just to be sure: take only behind comma
    hue = fractals * 0.7
    col = scale_tuple(hsv_to_rgb(hue, 1.0, 1.0))    # convert the hue to RGB values
    # logger.debug('- fractal is {}, hue is {}, rgb is {}, color is {}'
    #              .format(fractals, hue, rgb, col))
    return col


def draw_bomb():
    sense.set_pixels([bg_color] * 64)
    for t in bomb_pixel_array:
        sense.set_pixel(t[0], t[1], bomb_color1)
    # bomb light (should look "metallic"):
    sense.set_pixel(1, 2, bomb_color2)
    sense.set_pixel(2, 3, bomb_color2)


def draw_fuse_full():
    """Draw the full fuse (unburnt)"""
    for t in fuse_pixels:
        sense.set_pixel(t[0], t[1], cord_color)


def draw_fuse(len):
    """Draw only part of the fuse (given length)and draw
    the background for the remaining (already burnt) length.
    """
    for p in range(0, len):
        pix = fuse_pixels[p]
        sense.set_pixel(pix[0], pix[1], cord_color)
    for p in range(len, full_fuse_len):
        pix = fuse_pixels[p]
        sense.set_pixel(pix[0], pix[1], bg_color)


def blink_fuse(pixel, fuse_col):
    # for i in range(2):
    sense.set_pixel(pixel[0], pixel[1], black)
    sleep(0.05)
    sense.set_pixel(pixel[0], pixel[1], fuse_col)
    sleep(0.05)
    sense.get_accelerometer_raw()


def show_exploding_bomb():
    logger.info('bomb has exploded')
    draw_bomb()
    sleep_but_getaccelero(0.5)
    sense.clear(bomb_burnt_color)
    sleep_but_getaccelero(0.3)
    sense.clear(bg_color_exploded)
    for t in bomb_pixel_array:
        sense.set_pixel(t[0], t[1], bomb_burnt_color)
    show_burning_bomb()


def show_burning_bomb():
    for i in range(100):
        # get random pixel of bomb and paint it in random fire color
        pix = random.choice(bomb_pixel_array)
        col = random.choice(fire_colors)
        sense.set_pixel(pix[0], pix[1], col)
        sense.get_accelerometer_raw()
        sleep(0.05)


def get_burning_bomb_screen():
    """The same as show_burning_bomb(), but do it quickly in memory
    and then return the resulting screen."""
    screen = ([bg_color_exploded] * 64)
    for t in bomb_pixel_array:
        index = t[0] + t[1] * 8
        screen[index] = bomb_burnt_color
    for i in range(20):
        # get random pixel of bomb and paint it in random fire color
        pix = random.choice(bomb_pixel_array)
        col = random.choice(fire_colors)
        index = pix[0] + pix[1] * 8
        screen[index] = col
    return screen


def _test_fuse_colors():
    sense.clear()
    for i in range(9, -1, -1):
        remains = float(i)/10
        color = get_fusepixel_color(remains)
        logger.debug('remains is {}, color is {}'.format(remains, color))
        if i > 4:
            sense.set_pixel(i - 5, 1, color)
        else:
            sense.set_pixel(i, 3, color)


def _test_fire_colors():
    sense.clear()
    for i in range(len(fire_colors)):
        y = i // 8
        x = i % 8
        col = fire_colors[i]
        logger.debug('fire color #{} at position {}/{} is {}'
                     .format(i, x, y, col))
        sense.set_pixel(x, y, col)


def _test():
    sense.clear()
    sleep_but_getaccelero(1)
    draw_bomb()
    draw_fuse_full()

    blink_pixel = fuse_pixels[len(fuse_pixels) - 1]
    for i in range(9, -1, -1):
        remains = float(i)/10
        logger.debug('remains is {}'.format(remains))
        color = get_fusepixel_color(remains)
        blink_fuse(blink_pixel, color)

    show_exploding_bomb()


def test_random_bomb():
    screen = get_burning_bomb_screen()
    sense.set_pixels(screen)


if __name__ == "__main__":  # to start this: `python3 -m bombgame.countdown.displayUtils`
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-15s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=f)
    logger.setLevel(logging.DEBUG)

    sense.low_light = False
    # uncomment one line for a test:

    # draw_bomb()
    # show_burning_bomb()
    # test_random_bomb()
    # _test_fuse_colors()
    # _test_fire_colors()
    _test()
