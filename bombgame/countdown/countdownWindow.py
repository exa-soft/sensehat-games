"""
This module contains a window with a countdown that will not stop
running if the window is not displayed on the SenseHAT screen.
When the time is up, a "game over" event will be triggered (usually
the "explosion" of a bomb).
"""

__author__ = 'Edith Birrer'
__version__ = '1.0'


from math import floor
from threading import Thread
from time import sleep
from bombgame.core.common import SenseHat
from bombgame.core.exceptions import ArgumentError
from bombgame.core.game import GameWindow
from bombgame.countdown import displayUtils as dU
import logging

logger = logging.getLogger('countdown.window')
sense = SenseHat()

countdown_interval = 0.1
COUNTDOWN_ID = '!countdown!'

class CountdownWindow(GameWindow):

    time_left = 0.0     # this will only be changed by the countdown thread
    sec_per_fuse_pixel = 0.0
    _old_fuse_len = 0
    _countdown_thread = None
    _signalexpired_cbk = None   # callback function to signal that time is expired

    # overwritten from superclass (required):

    def init_game(self, **moreArgs):
        """Init countdown. Expects arguments 'duration' (how many seconds)
        and 'expired' (a handler from the grid that handles the "game over").

        :param moreArgs: Expected args: 'duration' (seconds), 'expired' (handler function)
        """
        dur = moreArgs['duration']
        if dur > 900:
            raise ArgumentError(len, 'duration must be <= 900')

        # init values
        self._old_fuse_len = dU.full_fuse_len
        self.time_left = float(dur)
        self.sec_per_fuse_pixel = self.time_left / dU.full_fuse_len

        # start the countdown timer in a separate thread
        # (a daemon - will die when main thread finishes)
        self._countdown_thread = Thread(target=self.countdown, daemon=True)
        self._countdown_thread.start()
        # logger.debug('{} threads: {}'.format(activeCount(), enumerate()))
        logger.info('CountdownWindow initialized (to {} seconds, countdown started)'.format(dur))
        logger.debug('- per fuse pixel: {}'.format(self.sec_per_fuse_pixel))

    def get_name(self):
        return 'countdown ({} seconds)'.format(self.time_left)

    def countdown(self):
        """Will be run in a separate thread and reduce the time_left
        """
        while self.time_left > 0:
            sleep(countdown_interval)
            self.time_left -= countdown_interval
        logger.info('==== time has expired! ====')
        self._signalexpired_cbk()

    def start_game(self):
        dU.draw_bomb()
        dU.draw_fuse_full()
        logger.debug('- start: per fuse pixel: {}'.format(self.sec_per_fuse_pixel))
        logger.debug('- start: full_fuse_len={}, old_fuse_len={}'
                     .format(dU.full_fuse_len, self._old_fuse_len))

    def play(self):
        """If the bomb is on screen, we have to display the fuse burning
        down and getting shorter.
        """
        # logger.debug('in play, (time remaining: {}'.format(self.time_left))
        # if self.is_solved():
        #     logger.info('bomb is solved!!! (time remaining: {})'.format(self.time_left))
        # elif self.running:
        if self.running:
            if self.time_left <= 0:
                # bomb has already exploded, display this in an animation (only used if played outside a grid)
                dU.show_burning_bomb()
                logger.info('bomb has already exploded!!! (time remaining: {})'.format(self.time_left))

            else:
                # Display bomb with fuse getting shorter.
                # number of unburnt fuse-pixels:
                fuse_len = floor(self.time_left // self.sec_per_fuse_pixel)
                if fuse_len < self._old_fuse_len:
                    # fuse just got shorter - re-paint it
                    dU.draw_fuse(fuse_len)
                # how many seconds left in the burning fuse-pixel:
                burn_sec_left = self.time_left - fuse_len * self.sec_per_fuse_pixel
                # scale this to standard length (value between 0 and 1):
                burn_left = burn_sec_left / self.sec_per_fuse_pixel
                fuse_col = dU.get_fusepixel_color(burn_left)
                dU.draw_fuse(fuse_len)
                # skip execution the very first time as if the fuse it is not yet lit
                # (we would have index out of range in dU.fuse_pixels)
                if fuse_len < dU.full_fuse_len:
                    dU.blink_fuse(dU.fuse_pixels[floor(fuse_len)], fuse_col)

    def is_solved(self):
        return False    # this game will never become solved

    # overwritten from superclass (optional):

    def pause_game(self):
        logger.debug('countdown: pause ({} sec. left)'.format(self.time_left))
        pass

    def continue_game(self):
        logger.debug('countdown: continue ({} sec. left)'.format(self.time_left))
        pass

    # def get_screen(self):
        # nothing, we use what is on the screen anyway
        # pass

    def get_solved_screen(self):
        """This "game" is solved when the bomb has exploded, so return
        the screen of a burning bomb. """
        return dU.get_burning_bomb_screen()

    def get_border_color(self):
        return 200, 0, 0

    # special for countdown: callback for time expiration

    def set_signalexpired_cbk(self, expired_function):
        """Set the function for signalling to the game grid that time has expired.
        This will be set when the game is inserted into the grid.
        :param expired_function:  the function to call when the time is up
        """
        self._signalexpired_cbk = expired_function


def _test_solved_callback(game_id):
    logger.info('game "{}" was solved'.format(game_id))
    sense.show_letter('!')


def _test():
    game1 = CountdownWindow('counter', duration=20)
    game1.set_signalsolved_cbk(_test_solved_callback)
    game1.resume_game()


if __name__ == "__main__":  # to start this: `python3 -m bombgame.countdown.countdownWindow`
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-15s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=f)
    logger.setLevel(logging.DEBUG)

    # _test_draw()
    _test()
