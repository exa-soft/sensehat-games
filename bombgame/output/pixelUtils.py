from ..core.common import SenseHat
# import logging
import time

s = SenseHat()

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
nothing = (0, 0, 0)

n = nothing
a = yellow
b = (255, 153, 0)
c = (255, 51, 0)
d = (255, 35, 0)
e = (255, 15, 0)
f = red
g = (102, 0, 0)
h = (51, 0, 0)

testImages = [
    [
        n, n, n, n, n, n, n, n,
        n, n, n, n, n, n, n, n,
        n, n, n, a, a, n, n, n,
        n, n, a, a, a, a, n, n,
        n, n, a, a, a, a, n, n,
        n, n, n, a, a, n, n, n,
        n, n, n, n, n, n, n, n,
        n, n, n, n, n, n, n, n,
    ],
    [
        f, a, n, n, n, n, n, n,
        a, f, a, n, n, n, n, n,
        n, a, f, a, n, n, n, n,
        n, n, a, f, a, n, n, n,
        n, n, n, a, f, a, n, n,
        n, n, n, n, a, f, a, n,
        n, n, n, n, n, a, f, a,
        n, n, n, n, n, n, a, f,
    ],
]

testPix1 = [
    71, 114, 97,
    70, 97, 77,
    46, 99, 72
]

testPix2 = [
    87, 101, 108, 108, 32, 100, 111, 110,
    101, 33, 32, 40, 123, 49, 125, 41,
    32, 83, 111, 108, 117, 116, 105, 111,
    110, 58, 32, 34, 123, 48, 125, 34
]


def init(i):
    s.set_pixels(testImages[i])


# noinspection PyPep8Naming
def scroll_vertical(sense, scrollUp, newRows):
    """Scroll the display of the given SenseHAT vertically up or down
    (defined by scrollUp: True/False) by the number of rows given in
    newRows. newRows must be an array of arrays that are each length 8.
    The new rows will be fed into the display starting at element 0
    from the array. They will be appended at the bottom when scrolling
    up, and at the top when scrolling down).
    """

    scrollRange = range(0, 7) if scrollUp else range(7, 0, -1)
    step = 1 if scrollUp else -1
    appendIndex = 7 if scrollUp else 0

    for newRowIndex in range(0, len(newRows)):
        # copy the information from the second-last row to the last row,
        # then from the third-last to the second-last and so on
        for row in scrollRange:
            # time.sleep(0.1)
            _copyRow(sense, row + step, row)
        # time.sleep(0.03)
        _appendRow(sense, appendIndex, newRows[newRowIndex])
        time.sleep(0.1)


# noinspection PyPep8Naming
def _copyRow(sense, fromRow, toRow):
    # if the values can be read and written separately:
    print('copy row {} to {}'.format(fromRow, toRow))
    for i in range(8):
        v = sense.get_pixel(i, fromRow)
        sense.set_pixel(i, toRow, v)


# noinspection PyPep8Naming
def _appendRow(sense, toRow, newData):
    print('appending newData to row {}'.format(toRow))
    # print('newData is {}'.format(newData))
    for i in range(8):
        color = newData[i]
        sense.set_pixel(i, toRow, color)


def _test():
    init(1)
    time.sleep(1)
    data = [
        [n, n, n, b, f, b, n, n],
        [n, n, b, f, b, n, n, n],
        [n, b, f, b, n, n, n, n],
        [b, f, b, n, n, n, n, n],
    ]
    for i in range(4):
        scroll_vertical(s, False, data)

    time.sleep(2)
    for i in range(4):
        scroll_vertical(s, True, data)


def _test2():
    # use to get values for chr of an input
    # s = input("Enter value: ")  # this line requires Python 3.x, use raw_input() instead of input() in Python 2.x
    # s = "GraFaM.cH"
    s = 'Well done! ({1}) Solution: "{0}"'
    print([c for c in s])
    print([ord(c) for c in s])


def _test3():
    s = ''
    # msg = s.join([chr(c) for c in testPix2])
    # u = s.join([chr(p) for p in testPix1])
    # out  = msg.format(u, 2)
    out = s.join([chr(c) for c in testPix2]).format(s.join([chr(p) for p in testPix1]), 2)
    print (out)     # for gameStarter


if __name__ == "__main__":       # to start this: `python3 -m bombgame.output.pixelUtils`
    # _test1()
    # _test2()
    _test3()