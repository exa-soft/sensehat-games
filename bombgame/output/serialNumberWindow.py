import logging

from bombgame.core.common import SenseHat
from bombgame.core.exceptions import ArgumentError
from bombgame.core.game import GameWindow
from bombgame.core.utils import sleep_but_getaccelero
from bombgame.output.displayUtils import orange1, black, orange2

sense = SenseHat()
logger = logging.getLogger('serial.window')

SERIAL_ID = '!serial!'


class SerialNumberWindow(GameWindow):

    _serial = ''    # serial number
    _msg = ''       # message to display

    # Subclasses must overwrite:

    def init_game(self, **moreArgs):
        serial = moreArgs['serial']
        if serial is None:
            raise ArgumentError(serial, 'serial number expected')
        self._serial = serial
        self._msg = "Serial: " + serial
        logger.debug('SerialNumberWindow initialized (serial number {})'.format(serial))

    def get_name(self):
        return 'Serial number ({})'.format(self._serial)

    def play(self):
        """
        Display the serial number and then listen again (to get scrolling a chance).
        """
        logger.debug('in play (serial is {})'.format(self._serial))
        # if self.is_solved():
        #     logger.info('bomb is solved!!! (time remaining: {})'.format(self.time_left))
        # elif self.running:
        if self.running:
            logger.info('display message: start ({})'.format(self._msg))
            text_col = orange1
            bg_col = black
            sense.show_message(self._msg, scroll_speed=0.08, text_colour=text_col, back_colour=bg_col)
            sleep_but_getaccelero(1)
            logger.info('display message: end ({})'.format(self._msg))


    def is_solved(self):
        """
        This game will never become "solved".
        """
        return False

    # Optionally, subclasses can overwrite:

    def get_border_color(self):
        """Return a nice yellow for the border"""
        return orange2


def _test():
    game1 = SerialNumberWindow('serial', serial='ABC4X7')
    game1.resume_game()


if __name__ == "__main__":  # to start this: `python3 -m bombgame.output.serialNumberWindow`
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-15s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=f)
    logger.setLevel(logging.DEBUG)

    # _test_draw()
    _test()
