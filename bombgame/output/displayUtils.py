"""
This module contains utils for manipulating the screen and for showing failure
or success, a solved module etc.
The method with names "draw" (or "clear") do not include waiting (they only change
pixels). The methods with names "show" (or "blink") include waiting (usually with
getting accelerometer values, so the queue does not fill with outdated values).
"""

import time
from typing import Tuple, List

from bombgame.core.common import SenseHat
from bombgame.core.exceptions import ArgumentError
from bombgame.core.utils import sleep_but_getaccelero

Color = Tuple[int, int, int]

# Possible colors are in the RGB-565-space (red 5 bits, green 6 bits, blue 5 bits).
# Therefore: red and blue part must be a multiple of 8, green part a multiple of 4.
black = (0, 0, 0)               # off, black        # all are of type Color
white_almost = (208, 208, 208)  # on, white (almost)
red1 = (216, 0, 0)          # red
red2 = (108, 0, 0)          # dark red
green1 = (0, 216, 0)        # green (bright)
green2 = (0, 144, 0)        # green (darker)
green3 = (0, 72, 0)         # green (dark)
orange1 = (248, 200, 40)    # orange (bright)
orange2 = (248, 160, 24)    # orange (darker)


# noinspection PyPep8Naming
def scroll_vertical(sense, scrollUp, newRows, speed=.1):
    """
    Scroll the display of the given SenseHAT vertically up or down
    by the number of rows given in newRows.
    The new rows will be fed into the display starting at element 0
    from the newRows array. They will be appended at the bottom when
    scrolling up, and at the top when scrolling down.
    :param sense:       where to display
    :param scrollUp:    True to scroll up, false to scroll down
    :param newRows:     new rows that appear when the screen is scrolled (an array of arrays that are each length 8)
    :param speed:       the time the display waits between scrolling steps
    """

    scrollRange = range(0, 7) if scrollUp else range(7, 0, -1)
    step = 1 if scrollUp else -1
    appendIndex = 7 if scrollUp else 0

    for newRowIndex in range(0, len(newRows)):
        # copy the information from the second-last row to the last row,
        # then from the third-last to the second-last and so on
        for row in scrollRange:
            copy_row(sense, row + step, row)
        append_row(sense, appendIndex, newRows[newRowIndex])
        time.sleep(speed)


# noinspection PyPep8Naming
def scroll_horizontal(sense, scrollLeft, newCols, speed=.1):
    """
    Scroll the display of the given SenseHAT horizontally left or right
    by the number of rows given in newRows.
    The new columns will be fed into the display starting at element 0
    from the newRow array. They will be appended at the right when
    scrolling left, and at the left when scrolling right.
    :param sense:       where to display
    :param scrollLeft:  True to scroll left, false to scroll right
    :param newCols:     new columns that appear when the screen is scrolled (an array of arrays that are each length 8)
    :param speed:       the time the display waits between scrolling steps
    """
    
    scrollRange = range(0, 7) if scrollLeft else range(7, 0, -1)
    step = 1 if scrollLeft else -1
    appendIndex = 7 if scrollLeft else 0

    for newColIndex in range(0, len(newCols)):
        # copy the information from the second-last col to the last col,
        # then from the third-last to the second-last and so on
        for col in scrollRange:
            copy_col(sense, col + step, col)
        append_col(sense, appendIndex, newCols[newColIndex])
        time.sleep(speed)


def copy_row(sense, fromRow, toRow):
    """
    Copy a row from the SenseHAT display to another row.
    :param sense:       where to display
    :type sense:        SenseHat
    :param fromRow:     row index (0..7)
    :type fromRow:      int
    :param toRow:       row index (0..7)
    :type toRow:        int
    :return:            None
    """
    for i in range(8):
        sense.set_pixel(i, toRow, sense.get_pixel(i, fromRow))


def copy_col(sense, fromCol, toCol):
    """
    Copy a column from the SenseHAT display to another column.
    fromCol and toCol are column indices (0..7)
    :param sense:       where to display
    :type sense:        SenseHat
    :param fromCol:     col index (0..7)
    :type fromCol:      int
    :param toCol:       col index (0..7)
    :type toCol:        int
    :return:            None
    """
    for i in range(8):
        sense.set_pixel(toCol, i, sense.get_pixel(fromCol, i))


def append_row(sense, toRow, newData) -> None:
    """Append data from an 8 element array with color values
    to a given row on the SenseHAT display.
    toRow must be a row index (0..7).
    newData must be an 8 element array with color tuples.
    """
    for i in range(8):
        sense.set_pixel(i, toRow, newData[i])


def append_col(sense: SenseHat, toCol: int, newData: List[Color]) -> None:
    """
    Append data from an 8 element array with color values
    to a given column on the SenseHAT display.
    :param sense:       SenseHAT
    :param toCol:       index of row where to write the data (0..7)
    :param newData:     8 element array with color values
    """
    for i in range(8):
        sense.set_pixel(toCol, i, newData[i])


# noinspection PyPep8Naming
def screen2rows(data: List[Color], fromTop: bool) -> List[List[Color]]:
    """
    Convert an array with screen data (64 elements)
    to an array of rows (8 elements with 8 elements each).
    :param data:        screen data (64 elements)
    :param fromTop:     defines if element 0 is topmost or bottommost row
    :return:            array of rows (8 elements with 8 elements each)
    """
    rowRange = range(8) if fromTop else range(7, -1, -1)
    return [data[y * 8:(y + 1) * 8] for y in rowRange]


# noinspection PyPep8Naming
def screen2cols(data: List[Color], fromLeft: bool) -> List[List[Color]]:
    """
    Convert an array with screen data (64 elements)
    to an array of columns (8 elements with 8 elements each).
    fromLeft defines if element 0 is left or rightmost column.
    :param data:        screen data (64 elements)
    :param fromLeft:    defines if element 0 is leftmost or rightmost column
    :return:            array of columns (8 elements with 8 elements each)
    """
    colRange = range(8) if fromLeft else range(8, 0)
    cols = [List[Color]] * 8
    for x in colRange:
        cols[x] = [data[y * 8 + x] for y in range(8)]
    return cols


def draw_full_screen(sense: SenseHat, color: Color) -> None:
    """
    Fills the whole display with the given color.
    :param sense:   SenseHAT
    :param color:   color to use
    """
    sense.clear(color)


def clear_screen(sense: SenseHat) -> None:
    """
    Clears the whole display (fills it with "black").
    :param sense:   SenseHAT
    """
    sense.clear()


def draw_corners(sense: SenseHat, color: Color) -> None:
    """
    Draws the corner pixels in the given color.
    :param sense:   SenseHAT
    :param color:   color to use
    :return:
    """
    sense.set_pixel(0, 0, color)
    sense.set_pixel(0, 7, color)
    sense.set_pixel(7, 0, color)
    sense.set_pixel(7, 7, color)


def draw_circle(sense: SenseHat, size: int, color: Color) -> None:
    """
    Draws a circle in the given color on the sensehat
    :param sense:   SenseHAT
    :param size:    Size of the circle (1 is inner square, 4 is maximum)
    :param color:   color for the display
    """

    if size < 1 or size > 4:
        raise ArgumentError(size, "size for circle must be between 1 and 4")

    if size == 1:       # display innermost square
        sense.set_pixel(3, 3, color)
        sense.set_pixel(4, 3, color)
        sense.set_pixel(3, 4, color)
        sense.set_pixel(4, 4, color)
    elif size == 2:
        for x in range(3, 5):
            sense.set_pixel(x, 2, color)
            sense.set_pixel(x, 5, color)
        for y in range(3, 5):
            sense.set_pixel(2, y, color)
            sense.set_pixel(5, y, color)
    elif size == 3:
        for x in range(2, 6):
            sense.set_pixel(x, 1, color)
            sense.set_pixel(x, 6, color)
        for y in range(2, 6):
            sense.set_pixel(1, y, color)
            sense.set_pixel(6, y, color)
    elif size == 4:
        for x in range(2, 6):
            sense.set_pixel(x, 0, color)
            sense.set_pixel(x, 7, color)
        for y in range(2, 6):
            sense.set_pixel(0, y, color)
            sense.set_pixel(7, y, color)
        sense.set_pixel(1, 1, color)
        sense.set_pixel(1, 6, color)
        sense.set_pixel(6, 1, color)
        sense.set_pixel(6, 6, color)
    else:
        pass


def draw_cross(sense: SenseHat, size: int, color: Color) -> None:
    """
    Draws a cross in the given color on the sensehat
    :param sense:   SenseHAT
    :param size:    Size of the cross (1 is 4x4 pixel, 3 is maximum)
    :param color:   color for the display
    """

    if size < 1 or size > 3:
        raise ArgumentError(size, "size for cross must be between 1 and 3")

    for i in range(0, size + 1):
        sense.set_pixel(3 - i, 3 - i, color)
        sense.set_pixel(4 + i, 3 - i, color)
        sense.set_pixel(3 - i, 4 + i, color)
        sense.set_pixel(4 + i, 4 + i, color)


def draw_circles(sense: SenseHat, a: Color, b: Color, c: Color, d: Color, e: Color) -> None:
    """
    Draws circles on the sensehat, a is the center color, b the circle around,
    c the 3rd circle, etc.
    :param sense:   SenseHAT
    :param a:   color for the center of the circles
    :param b:   color for the 2nd circle
    :param c:   color for the 3rd circle
    :param d:   color for the 4th circle
    :param e:   color for the corners
    """

    image = [
        e, d, d, d, d, d, d, e,
        d, d, c, c, c, c, d, d,
        d, c, c, b, b, c, c, d,
        d, c, b, a, a, b, c, d,
        d, c, b, a, a, b, c, d,
        d, c, c, b, b, c, c, d,
        d, d, c, c, c, c, d, d,
        e, d, d, d, d, d, d, e
    ]
    sense.set_pixels(image)


def display_growing_circles(sense: SenseHat, colors: List[Color], speed: float,
                            rep: int, intro: bool, outro: bool) -> None:
    """
    Display a show of circles in the given colors, growing from the center of the SenseHAT display and
    moving outwards (a negative speed moves them inwards).
    This is a very simple implementation which creates a big array of the repeated colors - so it should
    not be used with very big numbers of colors or repetitions.
    :param sense:   SenseHAT
    :param colors:  color list for the growing circles. First color is the center of the circles, second
        is displayed in the center when the first colors moves outward etc. List must contain at least
        one color (then the growing/shrinking is only visible if intro/outro is true). With two colors,
        growing and shrinking cannot be distinguished. Only with at least three colors, the effect is
        apparent. If the number of colors multiplied by the number of repetitions is smaller than 5 (i.e.
        does not fill the screen), the remaining circles will be black (even if intro/outro is false).
    :param speed:   How long (in seconds, max. 5) one position is displayed before the colors are moved to the
        next position. Recommended values between 0.1 and 0.5. Negative values will display shrinking circles.
        Speed 0 is not allowed.
    :param rep:     How many times to repeat the colors.
    :param intro:   True to start the animation from an empty screen. False to start the animation with a screen full
        of the first five color circles.
    :param outro:   True to end the animation with an empty screen. False to end the animation with a screen full
        of the last five color circles.
    """

    if len(colors) < 1:
        raise ArgumentError(colors, "colors must contain at least one Color")
    if speed == 0:
        raise ArgumentError(speed, "speed must not be 0")
    if rep <= 0:
        raise ArgumentError(rep, "rep (number of repetitions) must be a positive integer")

    # We repeat the color list by the number of repetitions (and do not care that this could be a very large list).
    # Then two cases:
    # 1) Repeated colorlist is longer than 5 -> use that directly
    # 2) Repeated colorlist is shorter than 5 -> pad the list with black (and shorten the outro/intro accordingly).
    colorlist = colors.copy() * rep    # with typing: colorlist: List[Color] = colors.copy() * rep
    trolen = max(5 - len(colorlist), 0)      # length of intro/outro already there - should not be negative
    for i in range(trolen):
        colorlist[0:0] = [_n]

    # append intro/outro
    if intro:
        colorlist = [_n] * (5 - trolen) + colorlist
    if outro:
        colorlist = colorlist + [_n] * 5

    for i in range(4, len(colorlist)):
        if speed > 0:   # speed > 0 -> color movement starts at the center, so earlier colors are towards the outside
            draw_circles(sense, colorlist[i], colorlist[i - 1], colorlist[i - 2], colorlist[i - 3], colorlist[i - 4])
            time.sleep(speed)
        else:   # speed < 0 -> colors movement starts at the corners, so earlier colors are in the center
            draw_circles(sense, colorlist[i - 4], colorlist[i - 3], colorlist[i - 2], colorlist[i - 1], colorlist[i])
            time.sleep(-speed)


def display_stroke(sense: SenseHat, clear_before: bool = True) -> None:
    """
    Display a red cross (pulsating a few times).
    Should be used as a signal to the user that this was a wrong input.
    Note that this method will not store nor restore the current screen.
    :param sense:   SenseHAT
    :param clear_before:   True to clear the display before (default True)
    """
    size = 2
    sleep_but_getaccelero(0.7)
    if clear_before:
        sense.clear()
    draw_cross(sense, size, red1)
    sleep_but_getaccelero(0.4)
    for i in range(0, 2):
        draw_cross(sense, size, red2)
        sleep_but_getaccelero(0.2)
        draw_cross(sense, size, red1)
        sleep_but_getaccelero(0.4)
    draw_cross(sense, size, black)
    sleep_but_getaccelero(0.2)


def display_correct(sense: SenseHat) -> None:
    """
    Display a green circle (growing and shrinking a few times).
    Should be used as a signal to the user that this was a correct input.
    Note that this method will not store nor restore the current screen.
    :param sense:   SenseHAT
    """
    circle_color = green2
    sleep_but_getaccelero(0.7)
    sense.clear()
    sleep_but_getaccelero(0.7)
    for i in range(0, 2):
        draw_circle(sense, 1, circle_color)
        sleep_but_getaccelero(0.15)
        for size in range(1, 4):
            draw_circle(sense, size + 1, circle_color)
            draw_circle(sense, size, black)
            sleep_but_getaccelero(0.15)
        for size in range(3, 0, -1):
            draw_circle(sense, size, circle_color)
            draw_circle(sense, size + 1, black)
            sleep_but_getaccelero(0.15)
        draw_circle(sense, 1, black)
        sleep_but_getaccelero(0.15)


def display_gamesolved(sense: SenseHat) -> None:
    """
    Displays that the game has been solved.
    :param sense:   SenseHAT
    """
    sleep_but_getaccelero(1)
    # colors = [_g1, _g2, _n]        # colors: List[Color]
    colors = [green1, _n, _n]  # colors: List[Color]
    sense.clear()
    display_growing_circles(sense, colors, 0.2, 3, True, True)  # (sense, colors, speed, rep, intro, outro)
    sleep_but_getaccelero(0.5)


def display_allsolved(sense: SenseHat) -> None:
    """
    Displays that the game has been solved.
    TODO to be decided what SimonSays should display (difference between solved "all games" or "this game").
    :param sense:   SenseHAT
    """
    sleep_but_getaccelero(1)
    colors = [green1, green2, green3]        # colors: List[Color]
    sense.clear()
    display_growing_circles(sense, colors, 0.2, 3, True, True)  # (sense, colors, speed, rep, intro, outro)
    sleep_but_getaccelero(0.5)


# test code ###########################################


_s = SenseHat()

# Possible colors are in the RGB-565-space (red 5 bits, green 6 bits, blue 5 bits).
# Therefore: red and blue part must be a multiple of 8, green part a multiple of 4.
_n = (0, 0, 0)            # off, black        # all are of type Color
_a = (255, 255, 0)  # yellow
_b = (255, 153, 0)  # orange
_f = (255, 0, 0)  # red


_testImages = [
    [
        _n, _n, _n, _n, _n, _n, _n, _n,
        _n, _n, _n, _n, _n, _n, _n, _n,
        _n, _n, _n, _a, _a, _n, _n, _n,
        _n, _n, _a, _a, _a, _a, _n, _n,
        _n, _n, _a, _a, _a, _a, _n, _n,
        _n, _n, _n, _a, _a, _n, _n, _n,
        _n, _n, _n, _n, _n, _n, _n, _n,
        _n, _n, _n, _n, _n, _n, _n, _n,
    ],
    [
        _f, _a, _n, _n, _n, _n, _n, _n,
        _a, _f, _a, _n, _n, _n, _n, _n,
        _n, _a, _f, _a, _n, _n, _n, _n,
        _n, _n, _a, _f, _a, _n, _n, _n,
        _n, _n, _n, _a, _f, _a, _n, _n,
        _n, _n, _n, _n, _a, _f, _a, _n,
        _n, _n, _n, _n, _n, _a, _f, _a,
        _n, _n, _n, _n, _n, _n, _a, _f,
    ],
]

_scrollData = [
    [_n, _n, _n, _b, _f, _b, _n, _n],
    [_n, _n, _b, _f, _b, _n, _n, _n],
    [_n, _b, _f, _b, _n, _n, _n, _n],
    [_b, _f, _b, _n, _n, _n, _n, _n],
]


def _init(i):
    _s.set_pixels(_testImages[i])


# noinspection PyPep8Naming
def _testVertical():
    print("in _testVertical")

    _init(1)
    time.sleep(1)
    for i in range(4):
        scroll_vertical(_s, False, _scrollData)

    time.sleep(2)
    for i in range(4):
        scroll_vertical(_s, True, _scrollData)


# noinspection PyPep8Naming
def _testHorizontal():
    print("in _testHorizontal ")

    _init(1)
    time.sleep(1)
    for i in range(4):
        scroll_horizontal(_s, False, _scrollData)

    time.sleep(2)
    for i in range(4):
        scroll_horizontal(_s, True, _scrollData)


def _test_circleandcross():
    _r  = (208, 0, 0)     # all are of type color: (with typing: _r: Color = (....) )
    _r2 = (104, 0, 0)
    _g  = (0, 208, 0)
    _g2 = (0, 104, 0)
    _w  = (208, 208, 208)

    time.sleep(1)

    for i in range(0, 2):
        draw_circle(_s, 1, _g)
        time.sleep(0.2)
        for size in range(1, 4):
            draw_circle(_s, size + 1, _g)
            # time.sleep(0.05)
            draw_circle(_s, size, _n)
            time.sleep(0.2)
        for size in range(3, 0, -1):
            draw_circle(_s, size, _g)
            # time.sleep(0.05)
            draw_circle(_s, size + 1, _n)
            time.sleep(0.2)
        draw_circle(_s, 1, _n)
        time.sleep(0.2)

    size = 3
    draw_cross(_s, size, _r)
    time.sleep(0.5)
    for i in range(0, 2):
        draw_cross(_s, size, _r2)
        time.sleep(0.2)
        draw_cross(_s, size, _r)
        time.sleep(0.5)
    draw_cross(_s, size, _n)


def _test_growing_circles():
    _g = (0, 208, 0)     # with typing: _g: Color = (0, 208, 0)
    _w = (208, 208, 208)
    testcolors = [_a, _b, _f, _g, _w, _f]   # with typing: testcolors: List[Color] = [...]
    speed = .2

    for collen in range(2, len(testcolors)):
        print("testing growing circles: collen {0}".format(collen))
        for rep in range(1, 5 // collen + 2):
            _s.clear()
            colors = testcolors[0:collen]
            print("- collen {0}, rep {1}: {2}".format(collen, rep, colors))
            display_growing_circles(_s, colors, speed, rep, True, True)
            _s.clear()
            time.sleep(1)


def _test_feedbacks():
    display_correct(_s)
    display_stroke(_s)
    display_gamesolved(_s)
    display_allsolved(_s)


if __name__ == "__main__":       # to start this: `python3 -m bombgame.output.displayUtils`
    # _testVertical()
    # _testHorizontal()
    # _test_circleandcross()
    # _test_growing_circles()
    _test_feedbacks()
