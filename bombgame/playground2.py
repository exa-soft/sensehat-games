# where I test a lot of things
# from enum import IntEnum, unique
# from typing import Tuple

import logging
from bombgame.core.common import SenseHat
from time import sleep
from signal import signal, SIGINT
from colorsys import hsv_to_rgb


# TODO should be moved to tests/manualtests

_s = SenseHat()


def exit_handler(sig, frame):
    """
    Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    # _s.clear()
    raise SystemExit


# Set up signal handler to catch Ctrl-c
signal(SIGINT, exit_handler)


# test code ###########################################


_green_hue = 0.33


def scale(v):
    return int(v * 255)


def show_rainbow():
    # Hues represent the spectrum of colors as values between 0 and 1. The range
    # is circular so 0 represents red, ~0.2 is yellow, ~0.33 is green, 0.5 is cyan,
    # ~0.66 is blue, ~0.84 is purple, and 1.0 is back to red. These are the initial
    # hues for each pixel in the display.
    hues = [
        0.00, 0.00, 0.06, 0.13, 0.20, 0.27, 0.34, 0.41,
        0.00, 0.06, 0.13, 0.21, 0.28, 0.35, 0.42, 0.49,
        0.07, 0.14, 0.21, 0.28, 0.35, 0.42, 0.50, 0.57,
        0.15, 0.22, 0.29, 0.36, 0.43, 0.50, 0.57, 0.64,
        0.22, 0.29, 0.36, 0.44, 0.51, 0.58, 0.65, 0.72,
        0.30, 0.37, 0.44, 0.51, 0.58, 0.66, 0.73, 0.80,
        0.38, 0.45, 0.52, 0.59, 0.66, 0.73, 0.80, 0.87,
        0.45, 0.52, 0.60, 0.67, 0.74, 0.81, 0.88, 0.95,
    ]
    while True:
        # Rotate the hues
        hues = [(h + 0.01) % 1.0 for h in hues]
        # Convert the hues to RGB values
        pixels = [hsv_to_rgb(h, 1.0, 1.0) for h in hues]
        # hsv_to_rgb returns 0..1 floats; convert to ints in the range 0..255
        pixels = [(scale(r), scale(g), scale(b)) for r, g, b in pixels]
        # Update the display
        _s.set_pixels(pixels)
        sleep(0.04)


def show_green():
    values = [
        0.00, 0.00, 0.06, 0.13, 0.20, 0.27, 0.34, 0.41,
        0.00, 0.06, 0.13, 0.21, 0.28, 0.35, 0.42, 0.49,
        0.07, 0.14, 0.21, 0.28, 0.35, 0.42, 0.50, 0.57,
        0.15, 0.22, 0.29, 0.36, 0.43, 0.50, 0.57, 0.64,
        0.22, 0.29, 0.36, 0.44, 0.51, 0.58, 0.65, 0.72,
        0.30, 0.37, 0.44, 0.51, 0.58, 0.66, 0.73, 0.80,
        0.38, 0.45, 0.52, 0.59, 0.66, 0.73, 0.80, 0.87,
        0.45, 0.52, 0.60, 0.67, 0.74, 0.81, 0.88, 0.95,
    ]
    while True:
        # Rotate the values
        values = [(v + 0.01) % 1.0 for v in values]
        # Convert the values to RGB values
        pixels = [hsv_to_rgb(_green_hue, v, 1.0) for v in values]
        # hsv_to_rgb returns 0..1 floats; convert to ints in the range 0..255
        pixels = [(scale(r), scale(g), scale(b)) for r, g, b in pixels]
        # Update the display
        _s.set_pixels(pixels)
        sleep(0.04)


def show_green2():
    saturs = [
        0.00, 0.00, 0.06, 0.13, 0.20, 0.27, 0.34, 0.41,
        0.00, 0.06, 0.13, 0.21, 0.28, 0.35, 0.42, 0.49,
        0.07, 0.14, 0.21, 0.28, 0.35, 0.42, 0.50, 0.57,
        0.15, 0.22, 0.29, 0.36, 0.43, 0.50, 0.57, 0.64,
        0.22, 0.29, 0.36, 0.44, 0.51, 0.58, 0.65, 0.72,
        0.30, 0.37, 0.44, 0.51, 0.58, 0.66, 0.73, 0.80,
        0.38, 0.45, 0.52, 0.59, 0.66, 0.73, 0.80, 0.87,
        0.45, 0.52, 0.60, 0.67, 0.74, 0.81, 0.88, 0.95,
    ]
    while True:
        # Rotate the values
        saturs = [(s + 0.01) % 1.0 for s in saturs]
        # Convert the values to RGB values
        pixels = [hsv_to_rgb(_green_hue, 1.0, s) for s in saturs]
        # hsv_to_rgb returns 0..1 floats; convert to ints in the range 0..255
        pixels = [(scale(r), scale(g), scale(b)) for r, g, b in pixels]
        # Update the display
        _s.set_pixels(pixels)
        sleep(0.04)


# Initialize
def init():
    print("in init")


if __name__ == "__main__":      # to start this: `python3 -m bombgame.playground2`
    logging.getLogger().setLevel(logging.INFO)
    init()
    # show_rainbow()
    # show_green()
    show_green2()
