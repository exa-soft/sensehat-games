import logging
import random
from .colors import WireColor
from .wiresSolution import getSolution, random_serial

logger = logging.getLogger('wires.simple')


# noinspection PyPep8Naming
class SimpleWires(object):
    """
    Class for a game of SimpleWires with 3 or 4 wires
    To play a game:
    - init the object with a random serial key (digits and letters) - if not given, it will be created with random_serial(size=6)
    - call `.restart()` to start the game - this will display
    - listen to user input (which wire to cut) and give it (as a number between 0..2 or 0..3) to `.cut_cire(the number)`

    Important:
    - The object will not contain a solution before `restart()` is called for the first time.
    See also example in  `test()` or the example implementation `simonOnTurtle`.
    Subclasses should overwrite:
    - `onRestart()`
    - `nextTry()`
#    - `_displayUncutWire(index)` and `_displayCutWire(index)`
    - `displayWrongWire()`
    - `displayGameSolved()`
    - `displayGameLost()`
    If the game is embedded with some others which also cause anc  ount strikes,
    the method `maxStrikesReached` should also be overwritten.
    """

    _size = -1          # _size: int
    _wires = []         # _wires: List[Color]
    _cut = []           # _cut: List[bool]
    _serial = ''        # _serial: str
    _solution = -1      # _solution: int
    _solved = False     # _solved: bool
    _strikesLeft = 3    # how many wrong attempts are required to lose the game

    def __init__(self, serial = random_serial(size=6)):
        """
        Init a SimpleWires game
        :param serial:  serial number, optional (if not given: generates a random one of length 6)
        """
        self._serial = serial
        # other variables will be initialized in restart()

    def _reset(self):
        """
        Resets the internal variables (new solution).
        :param serial:  serial number, optional (if not given: generates a random one of length 6)
        """
        logger.debug('_reset')
        self._size = random.randint(3, 4)   # random 3 or 4
        self._wires = [WireColor.BLACK] * self._size    # will be overwritten below
        for i in range(self._size):
            colorNum = random.randint(0, len(WireColor) - 1)
            # color = random.choice(Color)
            color = WireColor(colorNum)
            logger.debug('- color: {}'.format(color))
            self._wires[i] = color
        self._cut = [False] * self._size
        self._solution = getSolution(self._wires, self._serial)
        logger.info("initialized wire game: {}".format(self))

    def restart(self):
        """
        Start or restart the game (with a new solution).
        """
        logger.debug('in restart...')
        self._reset()
        logger.debug('restart: will call onRestart()')
        self.onRestart()
        logger.debug('restart: will call displayWires()')
        self.displayWires()
        logger.debug('restart: will call nextTry()')
        self.nextTry()

    def onRestart(self):
        """
        If something beside displaying the wires must be done, for a new game,
        Subclasses can overwrite this method to do additional things on startup.
        """
        # display of wires must be done by subclass
        pass

    def displayWires(self):
        """
        Display the wires.
        This method must be overwritten by the subclass.
        """
        for i in range(self._size):
            if self._cut[i]:
                self._displayCutWire(i)
            else:
                self._displayUncutWire(i)

    def _displayUncutWire(self, index: int):
        """
        Display one wire that is not cut.
        :param index: index of wire to display.
        """
        color = self._wires[index]
        logger.debug('- wire {}: {}'.format(index, color))

    def _displayCutWire(self, index: int):
        """
        Display one wire that is cut.
        :param index: index of wire to display.
        """
        color = self._wires[index]
        logger.debug('- cut  {}: {}'.format(index, color))

    def maxStrikesReached(self) -> bool:
        """
        Return if the maximum number of strokes haven been reached.
        Default implementation: game is lost at the third wrong attempt.
        TODO maybe this must be implemented (also) for GameGrid
        """
        logger.debug('strikes left: {}'.format(self._strikesLeft))
        return self._strikesLeft == 0

    def nextTry(self):
        """
        Listen for user input (which wire to cut). Then cutWire with the received input.
        Subclasses must overwrite this method and give the user input to self.cutWire(index).
        They must not call self.nextTry() - this will be done by self.cutWire!
        """
        logger.debug('- which wire to cut?')

    def cutWire(self, index: int):
        """
        The game receives a cut of a wire (0..3).
        Subclasses must not overwrite this method, but call it with an input received from self.nextTry().
        If index is invalid or the wire is already cut, .displayCuttingInvalid() will be called.
        Otherwise, .displayCuttingWire() will be called.
        Then, if the correct wire has been cut, .displayGameSolved() will be executed.
        Otherwise, if a wrong wire has been cut and the maximal number of strikes has
        already been reached, the game is lost and .displayGameLost() will be executed.
        If there are more strokes left, the wire will be marked as cut and .displayWrongWire()`
        followed by .nextTry() will be called.
        :param index:   which wire to cut (0..3 or 0..2, depending on number of wires)
        """
        logger.debug('try to cut wire: {}'.format(index))
        if index < 0 or index >= self._size:
            logger.warning('- cutWire: {} is an invalid index for a wire'.format(index))
            self.displayCuttingInvalid(index)
            self.nextTry()
            return
        if self._cut[index]:
            logger.debug('- cutWire: wire {} is already cut'.format(index))
            self.displayCuttingInvalid(index)
            self.nextTry()
            return

        # cut the wire
        self._cut[index] = True
        self.displayCuttingWire(index)
        logger.debug('- cutWire: wire {} has been cut'.format(index))
        if index + 1 == self._solution:     # solution is a position (1-based), so + 1
            # was correct wire
            logger.debug('- cutWire: wire {} was correct solution'.format(index))
            self._solved = True
            self.displayGameSolved()
            return
        # was wrong wire
        self._strikesLeft -= 1
        logger.debug('- cutWire: wire {} was NOT correct solution! Strikes left: {}'
                     .format(index, self._strikesLeft))
        if self.maxStrikesReached():
            self.displayGameLost()
        else:
            self.displayWrongWire()
            self.nextTry()

    def displayCuttingInvalid(self, index: int):
        """
        If necessary, subclasses can overwrite this method to do something if the cut operation
        was done on an invalid position (there was no wire or it was already cut).
        The default implementation does nothing.
        :param index:   wire index that was tried to cut
        """
        logger.debug('- invalid index for wire: {}'.format(index))

    def displayCuttingWire(self, index: int):
        """
        Display how the wire is cut (independently if it was the correct wire or not).
        Must be overwritten by subclass.
        :param index:   which wire to cut (0..3 or 0..2, depending on number of wires)
        """
        logger.debug('- wire {} is being cut...'.format(index))

    def displayWrongWire(self):
        """
        Display feedback to the user that a wrong wire has been cut.
        Must be overwritten by subclass.
        IMPORTANT: Must not test for maxStrokesReached nor call nextTry - this is done in simpleWires.nextTry()!
        """
        logger.debug('- WRONG WIRE has been cut!')
        # display of failure must be done by subclass

    def displayGameSolved(self):
        """
        Display feedback to the user that this game is solved.
        Must be overwritten by subclass.
        """
        logger.debug('- GAME SOLVED!')

    def displayGameLost(self):
        """
        Display feedback to the user that this game is lost.
        Must be overwritten by subclass.
        When used in a grid, it is possible that this is not displayed at all
        (instead, the bomb explosion could be shown right away).
        """
        logger.debug('- GAME LOST!')

    def __str__(self):
        return 'SimpleWires (wires: {s._wires}, serial: {s._serial}, solution {s._solution})'\
            .format(s=self)


# noinspection PyPep8Naming
def _testAuto():
    wireGame = SimpleWires('a4b6c')
    wireGame.restart()
    print('[testout]', wireGame)


def _test():
    _testAuto()


if __name__ == "__main__":      # to start this: `python3 -m bombgame.simplewires.simplewires`
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.INFO, format=f)
    logger.setLevel(logging.WARNING)
    _test()
