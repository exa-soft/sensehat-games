import logging
import random
import string
from typing import List

from .colors import WireColor

logger = logging.getLogger('wires.solution')


def random_serial(size=8, chars=string.ascii_uppercase + string.digits, must_contain_digit=True) -> str:
    """
    Create a random serial number.
    :param size:    length of the number to generate
    :param chars:   character set to choose from (if must_contain_digit = True, string.digits will be added to chars if not already it there)
    :param must_contain_digit: if True, the number is guaranteed to contain at least one digit
    :return:    the random serial number
    """
    if must_contain_digit:
        if not chars.__contains__(string.digits):
            chars = chars + string.digits
    else:
        return ''.join(random.choice(chars) for _ in range(size))

    # we want a serial number with a digit, so keep generating randoms until we have at least one digit in it
    temp_serial = ''
    last_digit = ''
    while last_digit == '':
        temp_serial = ''.join(random.choice(chars) for _ in range(size))
        last_digit = _getLastDigit(temp_serial)
    return temp_serial


# noinspection PyPep8Naming
def getSolution(colorList: List[WireColor], serial: str) -> int:
    """Get the number of the wire to cut to solve the game (first is 1)
    :arg colorList  List of colors (ints)
    :arg serial     serial number (string)
    :return: number of the correct wire to cut (0..2 or 0..3)
    """

    if len(colorList) == 3:
        return getSolutionFor3(colorList)
    elif len(colorList) == 4:
        return getSolutionFor4(colorList, serial)
    else:
        return -1   # or raise error?


# noinspection PyPep8Naming
def getSolutionFor3(colorList: List[WireColor]) -> int:
    """Get the number of the wire to cut to solve the game (for 3 wires)"""
    if not _contains(WireColor.RED, colorList):
        logger.debug("Rule 3.1: no red wires -> cut number 2")
        return 2
    if colorList[2] == WireColor.WHITE:
        logger.debug("Rule 3.2: last is white -> cut number 3")
        return 3
    if _count(WireColor.BLUE, colorList) > 1:
        lastPos = _lastPos(WireColor.BLUE, colorList)
        logger.debug("Rule 3.3: more than one blue -> cut the last blue ({})".format(lastPos))
        return lastPos
    logger.debug("Rule 3.4, cut number 3")
    return 3


# noinspection PyPep8Naming
def getSolutionFor4(colorList: List[WireColor], serial: str) -> int:
    """Get the number of the wire to cut to solve the game (for 4 wires)"""
    if _count(WireColor.RED, colorList) > 1 and _isLastDigitOdd(serial):
        logger.debug("Rule 4.1: more than one red and last serial digit is odd -> cut last red")
        return _lastPos(WireColor.RED, colorList)
    if colorList[3] == WireColor.YELLOW and _count(WireColor.RED, colorList) == 0:
        logger.debug("Rule 4.2: last is yellow and no red -> cut first")
        return 1
    if _count(WireColor.BLUE, colorList) == 1:
        logger.debug("Rule 4.3: exactly one blue -> cut first")
        return 1
    if _count(WireColor.YELLOW, colorList) > 1:
        logger.debug("Rule 4.4: more than one yellow -> cut last")
        return 4
    logger.debug("Rule 4.5: else -> cut number 2")
    return 2


def _contains(color: int, colorList: List[int]) -> bool:
    """Checks if a wire of the given color is in the list"""
    for col in colorList:
        if col == color:
            return True
    return False


def _count(color: int, colorList: List[int]) -> int:
    """Count how many wires of the given color are in the list"""
    number = 0
    for col in colorList:
        if col == color:
            number = number + 1
    return number


# noinspection PyPep8Naming
def _getLastDigit(serial: str) -> str:
    """
    Get the last digit in the given serial number. If there is no digit, return an empty string
    :param serial:  serial number
    :return:  last digit as string
    """
    digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    digitsFromSerial = ''.join((filter(lambda x: x in digits, serial)))
    if len(digitsFromSerial) == 0:
        return ''
    return digitsFromSerial[len(digitsFromSerial) - 1]


# noinspection PyPep8Naming
def _isLastDigitOdd(serial: str) -> bool:
    last = _getLastDigit(serial)
    if last == '':
        return False
    digit = int(last)
    return (digit % 2) == 1


# noinspection PyPep8Naming
def _lastPos(color: int, colorList: List[int]) -> int:
    """Finds the last occurrence of the given color in the list"""
    lastPos = -1
    for i in range(len(colorList)):
        if colorList[i] == color:
            # logger.debug("found color {} at position {}".format(color, i))
            lastPos = i + 1
    return lastPos

# tests are in separate file, run them with (must be absolute path)
# python -m unittest /data/DevelopmentEB/Python/sensehat-games/tests/simplewires/test_wiresSolution.py
# (does not work: python -m unittest ./tests/simplewires/test_wiresSolution.py)
