import logging

from bombgame.core.common import SenseHat
from bombgame.output import displayUtils as dUt
from bombgame.output.displayUtils import Color
from bombgame.simplewires import displayWires as dW
from bombgame.simplewires.wiresInclinationMeter import WiresInclinationMeter
from bombgame.simplewires.simplewires import SimpleWires
from bombgame.simplewires.wiresSolution import random_serial

sense = SenseHat()
logger = logging.getLogger('wires.inclin')


class WiresOnInclination(SimpleWires):

    _meter = None           # _meter: InclinationMeter
    _run_in_gamewindow = True
    # if this is True, listen will not call a loop, but only check once
    # (looping must then be done externally, like by the GameWindow)
    _listening = False      # _listening: bool
    solved_screen = [Color] * 64   # cache screen of solved game, only valid when _solved = True

    def __init__(self, serial = random_serial(size=6)) -> None:
        SimpleWires.__init__(self, serial)
        # meter will be re-initialized on restart
        self._meter = WiresInclinationMeter(sense, [], self._startCutWire)
        logger.info('WiresOnInclination initialized: {}, solution is '.format(self))

    def onRestart(self) -> None:
        """
        Reset screen on restart
        (overwritten method from SimonSays)
        """
        logger.debug('in onRestart...')
        wirePositions = dW.get_columns_for_size(self._size)
        self._meter = WiresInclinationMeter(sense, wirePositions, self._startCutWire)
        # self._meter.clear()
        dW.display_wires(sense, self._wires, self._cut)
        start_cross_pos = 3 if self._size == 4 else 4       # at start, cross should not be on a wire
        self._meter.show_cross(start_cross_pos)
        self._listening = True
        logger.info("neue Lösung bereit (Länge: {}): {}".format(self._size, self._solution))

    def displayWires(self):
        """
        Display the wires: draws them.
        """
        dW.display_wires(sense, self._wires, self._cut)

    def listen(self) -> None:
        """Listen to user input.
        This checks the inclination values in a loop, running while
        isListening() returns true and the game is not yet solved.
        Base class says:
        "Subclasses must overwrite this method and give the user input to self.cutWire(index)."
        -> this is done in this subclass through self._meter.update() that will call self.cutWire
        via cut_callback when a wire is cut.
        """
        # if self.isListening() and not self._solved:
        if self._listening and not self._solved:
            self._meter.update()    # measure direction and steepness and display it
            # sleep_but_getaccelero(0.2)   # do not sleep - constantly measure and display!
            if not self._run_in_gamewindow:     # if not in a GameWindow, we have to do the looping
                self.listen()

    def nextTry(self):
        logger.debug('in nextTry()...')
        self._meter.hide_cross()
        self.displayWires()
        self._meter.show_cross()
        self._listening = True
        self.listen()

    def displayCuttingInvalid(self, index: int):
        # we should never try to cut an invalid wire, but if we do, log it
        logger.warning('- tried to cut an invalid wire: index {}'.format(index))

    def displayCuttingWire(self, index: int):
        """
        This method will be called when a wire cutting has been detected by the accelerometer and the cut move
        has already been displayed (the cross is back in its original position and still displayed as active,
        the pixel in the middle has background color).
        :param index:   index of wire being cut (0..2 or 0..3)
        """
        logger.debug('- display cutting wire {}'.format(index))
        # not much to do, wire is cut already, hide cross so it is clearly visisble
        dUt.sleep_but_getaccelero(0.2)
        self._listening = False
        self._meter.hide_cross()

    def displayWrongWire(self):
        """
        Feedback to the user that this was a wrong wire:
        display "stroke" signal
        """
        logger.debug('in displayWrongWire()...')
        dUt.display_stroke(sense, False)    # False: do not clear display before
        self.displayWires()
        dUt.sleep_but_getaccelero(0.3)
        self._meter.show_cross()
        self._listening = True

    def displayGameSolved(self):
        logger.debug('in displayGameSolved()...')
        # play "correct" animationE
        dUt.display_gamesolved(sense)
        # then display again the wires and place a green circle over it (as "solved" signal)
        self.displayWires()
        dUt.draw_circle(sense, 3, dUt.green1)
        # if the final screen has not yet saved, save it
        self.solved_screen = sense.get_pixels()
        logger.debug('cached solved screen: {}'.format(self.solved_screen))

    def displayGameLost(self):
        logger.debug('in displayGameLost()...')
        # TODO will this call the bomb?

    def _startCutWire(self, col: int):
        """
        This displays how the wire is cut and then calls the cutWire from the base class
        to do the cutting and see what happens afterwards (if correct wire etc.)
        :param col:     column of the wire that is cut
        """
        logger.debug('! cut wire at column {} !'.format(col))
        wirePos = dW.pos_for_column(self._size, col)
        logger.debug('will cut wire number {}'.format(wirePos))
        self.cutWire(wirePos)


def _test_display():
    _s = SenseHat()
    woi = WiresOnInclination()
    woi._run_in_gamewindow = False      # for testing outside of a GameWindow
    woi.restart()
    import time
    time.sleep(1)
    woi.displayWrongWire()
    woi.nextTry()
    time.sleep(1)
    woi.displayGameLost()
    time.sleep(1)
    woi.displayGameSolved()
    time.sleep(1)


def _test():
    _s = SenseHat()
    woi = WiresOnInclination()
    woi._run_in_gamewindow = False      # for testing outside of a GameWindow
    woi.restart()


def exit_handler(sig, frame):
    """
    Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    # _s.clear()
    raise SystemExit


if __name__ == "__main__":      # to start this: `python3 -m bombgame.simplewires.wiresOnInclination`
    # Set up signal handler to catch Ctrl-c
    from signal import signal, SIGINT
    signal(SIGINT, exit_handler)

    f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.INFO, format=f)
    logger.setLevel(logging.DEBUG)
    # logger.addHandler(logging.StreamHandler())
    # logging.getLogger('wires.meter').setLevel(logging.DEBUG)
    logging.getLogger('wires.simple').setLevel(logging.DEBUG)
    # logging.getLogger('wires.display').setLevel(logging.DEBUG)

    # _test_display()
    _test()

