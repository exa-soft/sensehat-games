"""
Class that can display a "cross" around a pixel (one pixel above and below, one pixel
left and right) and move that sideways depending on the inclination of the SenseHAT.
It will store the pixels that are overwritten by the cross and restore them when the
cross moves away.
On a quick move forward/backward, a "wire is cut" if one is there.
"""

import logging
from math import fabs
from typing import List, Callable

from bombgame.accele_input.inclinationMeter import InclinationMeter, Direction, Steepness, get_inclination
from bombgame.core.common import SenseHat
from bombgame.core.exceptions import ArgumentError
from bombgame.output import pixelUtils
from bombgame.output.displayUtils import Color
from .displayWires import cross1, cross2, color_off

logger = logging.getLogger('wires.meter')


class WiresInclinationMeter(InclinationMeter):
    """
    Class to wrap the analysis and display of the inclination of a SenseHAT
    for left/right moves (e.g. as used in the game SimpleWires).
    When a wire cutting operation is detected, a callback is raised (method that takes a column as parameter).
    """
    #                       # with typing
    _steps = 0              # int
    _showsCross = False     # bool
    _crossPos = 0           # int
    _pixels = [(0, 0, 0)] * 4        # Color array of len 4, the pixels that are covered by the cross
    # _pixels and _crossPos are only valid if _showsCross = True
    _wirePos = []           # List[int]
    _isCutting = False      # bool      # true if we are on a wire and moved down for cutting
    _cutCallback = None     # Callable[[int], None]

    def __init__(self, sensehat: SenseHat, wirePos: List[int], call_when_cut: Callable[[int], None]):
        """
        Init the meter, storing the positions of the wires
        :param wirePos:     List with 3 or 4 wire positions (each in 0..7)
        :param call_when_cut: function to be called when a wire has been cut.
            Function must take a SenseHAT and the position of the cut wire (as column 0..7)
        """
        InclinationMeter.__init__(self, sensehat)
        self._wirePos = wirePos
        self._cutCallback = call_when_cut
        self.clear()

    def __str__(self) -> str:
        return 'WiresInclinationMeter (cross on {sf._crossPos}, wires on {sf._wirePos})'.format(sf=self)

    def clear(self) -> None:
        """
        Clear the screen and reset the internal variables.
        Screen will not be cleared.
        """
        self._steps = 0
        self._showsCross = False
        self._crossPos = 0
        self._pixels = [(0, 0, 0)] * 4

    def show_cross(self, column: int = None) -> None:
        """
        This switches the cross on. If it is already on, nothing will be done.
        So this method must not be used to move the cross.
        If the
        :param column:  column where to display the cross. If not given, the old one will be used.
        """
        if self._showsCross:
            logger.warning('show_cross: cross already visible, skipping')
            return
        if column is None:
            column = self._crossPos
        if column < 0 or column > 7:
            raise ArgumentError(column, 'column to display cross must be >= 0 and <= 7')

        self._pixels = self._getCrossPixels(column)
        self._crossPos = column
        self._displayCross()
        self._showsCross = True

    def hide_cross(self) -> None:
        """
        This switches the cross off. If it is already off, nothing will be done.
        """
        if not self._showsCross:
            logger.warning('show_cross: cross already hidden, skipping')
            return

        self._restoreCrossPixels(self._pixels, self._crossPos)
        self._showsCross = False

    # noinspection PyPep8Naming
    def _getCrossPixels(self, column: int, row: int = 3) -> List[Color]:
        """
        Gets the colors of the pixels that would be under
        the cross at the given column (in order top, right, bottom, left).
        Row can also be given (e.g. to store pixels in the "cut" move),
        but default is normal cross position (row 3).
        :param column:  column of cross position (0..7)
        :param row:     row of cross position (default 3, can also be 4)
        :return:        list (with 4 elements) with cross pixels of given position (in order top, right, bottom, left)
        """
        if row < 3 or row > 4:
            raise ArgumentError(row, 'row to get cross pixels must be 3 or 4')
        pixels = [(0, 0, 0)] * 4
        pixels[0] = self._sense.get_pixel(column, row - 1)
        pixels[2] = self._sense.get_pixel(column, row + 1)
        if column < 7:
            pixels[1] = self._sense.get_pixel(column + 1, row)
        if column > 0:
            pixels[3] = self._sense.get_pixel(column - 1, row)
        return pixels

    # noinspection PyPep8Naming
    def _restoreCrossPixels(self, oldPixels: List[Color], oldColumn: int, oldRow: int = 3) -> None:
        """
        Restores the pixels that were hidden under the cross, using the given pixels.
        :param oldPixels:   the old covered pixels (as stored in self._pixels)
        :param oldColumn:   the old cross position x
        :param oldRow:      the old cross position y (default 3, can also be 4)
        """
        if oldRow < 3 or oldRow > 4:
            raise ArgumentError(oldRow, 'oldRow to restore cross must be 3 or 4')

        self._sense.set_pixel(oldColumn, oldRow - 1, oldPixels[0])
        self._sense.set_pixel(oldColumn, oldRow + 1, oldPixels[2])
        if oldColumn < 7:
            self._sense.set_pixel(oldColumn + 1, oldRow, oldPixels[1])
        if oldColumn > 0:
            self._sense.set_pixel(oldColumn - 1, oldRow, oldPixels[3])

    # noinspection PyPep8Naming
    def _displayCross(self, active: bool = False) -> None:
        """
        Paints the cross pixels at the current column. If active, they are painted
        brighter, otherwise normal.
        if _isCutting, the cross is at row 4, otherwise at row 3.
        """
        crossRow = 4 if self._isCutting else 3
        logger.debug('_displayCross (active: {1}) in row/column {2}/{0}'
                    .format(self._crossPos, active, crossRow))
        color = cross2 if active else cross1
        self._sense.set_pixel(self._crossPos, crossRow - 1, color)
        self._sense.set_pixel(self._crossPos, crossRow + 1, color)
        if self._crossPos < 7:
            self._sense.set_pixel(self._crossPos + 1, crossRow, color)
        if self._crossPos > 0:
            self._sense.set_pixel(self._crossPos - 1, crossRow, color)

    def _startCutMove(self) -> None:
        """
        Points the cross one position below the usual one and sets _isCutting to True
        (if already _isCutting, nothing will be done).
        n detail, this method will temporarily store the pixels one below the current cross pixels,
        then paint the cross at row 4, then use the stored pixels to clear the cross at row 3
        (this works because the wires are vertical and so all pixels of a column have the same color).
        """
        if self._isCutting:
            logger.warning('startCutMove: already isCutting, skipping')
            return

        tmpPixels = self._getCrossPixels(self._crossPos, 4)
        self._isCutting = True  # this causes the cross to be painted one row below
        self._displayCross(True)
        self._restoreCrossPixels(tmpPixels, self._crossPos, 3)

    def _endCutMove(self) -> None:
        """
        Clears the pixel in the middle of the cross and then paints the cross at the usual position,
        again with a background pixel in the middle. But switching off the cross is left to the caller
        (because the caller does all the timing of the display).
        This method also sets _isCutting to False (if not _isCutting, nothing will be done).
        In detail, this method will clear the pixel in the middle of the cross, then temporarily store the pixels one position
        above the current cross pixels, then paint the cross at row 3, then use the stored pixels to clear the cross at row 4
        (this works because the wires are vertical and so all pixels of a column have the same color).
        If the cross is switched off, a "hole" in the wire will be visible (and basically, painting the cut wire
        will not be necessary, the cross moving has already cleared these two pixels).
        However, switching the cross off must be done by the caller.
        """
        if not self._isCutting:
            logger.warning('endCutMove: not yet isCutting, skipping')
            return

        tmpPixels = self._getCrossPixels(self._crossPos, 3)
        self._sense.set_pixel(self._crossPos, 4, color_off)      # clear the pixel in the middle of the cross
        self._pixels[2] = color_off     # also clear the pixel in the temporary store
        # in the stored pixels, also clear the one at the top, so this leaves a "hole" in the center when restoring)
        tmpPixels[0] = color_off
        self._isCutting = False     # this causes the cross to be painted at the normal row
        self._displayCross(True)    # paint the active cross at the usual position
        self._restoreCrossPixels(tmpPixels, self._crossPos, 4)

    def update(self) -> None:
        """
        Measures the inclination of the SenseHAT and if it is sideways (pitch)
        and over a threashold, the cross is moved in this direction.
        If on a wire and the SenseHat is moved forward/backward, it returns true
        (thus signalling that "the wire has been cut".
        """

        incli = get_inclination(self._sense)   # incli: Inclination
        direction = incli[0]  # direction
        steep = incli[1]    # steepness
        if direction == Direction.NONE or steep < Steepness.LOW:
            # logger.debug('- update: no direction pr öpw steepness, skipping'.format(steep))
            return

        # if we are cutting, ignore sideways movements, only listen for BACKWARD to end cut
        if self._isCutting:
            if direction == Direction.BACKWARD and steep >= Steepness.MIDDLE:
                logger.debug('- update: direction {} and steepness {}, ending to cut'.format(direction, steep))
                self._endCutMove()
                # remove the current wire from the wires list (it cannot be cut twice)
                self._wirePos.remove(self._crossPos)
                # call the callback method to signal the cutting to the caller
                self._cutCallback(self._crossPos)
                return
            else:
                # during cutting, ignore sideways movements
                logger.debug('- update: direction is {}, but during cutting we will only listen for BACKWARD move'
                             .format(direction))
                return

        # not cutting:
        # if we are on a wire and steeply moving forward, start cut
        onWire = self._crossPos in self._wirePos
        if onWire and direction == Direction.FORWARD and steep >= Steepness.MIDDLE:
            logger.debug('- update: direction {} and steepness {}, starting to cut'.format(direction, steep))
            self._startCutMove()
            return

        # not starting nor ending cut -> ignore forward/backward movements
        if direction == Direction.FORWARD or direction == Direction.BACKWARD:
            return

        # not cutting and not moving backward/forward: do sideways moves:

        # if we are at an edge and want to go further sideways, do nothing
        # (code would work without these filters, but we do not need to make the all calculations below)
        if direction == Direction.LEFT and self._crossPos == 0:
            logger.debug('- update: direction is {}, but we are at the edge'.format(direction))
            return
        if direction == Direction.RIGHT and self._crossPos == 7:
            logger.debug('- update: direction is {}, but we are at the edge'.format(direction))
            return

        # move sideways: add steps depending on steepness and direction
        dir_factor = 1 if direction == Direction.RIGHT else -1     # -1 for left, +1 for right
        if steep == Steepness.LOW:
            stepsize = 1
        elif steep == Steepness.MIDDLE:
            stepsize = 2
        else:
            stepsize = 3    # Steepness.NEUTRAL hase been covered above, will not reach this code
        self._steps = self._steps + dir_factor * stepsize
        logger.debug('- update: (steepness {1}, direction {2}), added steps: now {0}'.format(
            self._steps, steep, direction))

        # if we have enough steps, move the cross
        threashold = 6
        if fabs(self._steps) < threashold:
            return
        # try to move right if steps positive, or left if steps negative
        go_right = (self._steps > 0)
        self._steps = 0
        self._move_cross(go_right)

    def _move_cross(self, move_right: bool) -> None:
        """
        Moves the cross one column to the left or right and adapts all internal variables.
        :param move_right:     true to move right, false to move left
        """
        old_pixels = self._pixels       # store the old covered pixels
        old_pos = self._crossPos        # store the old position
        new_pos = old_pos + 1 if move_right else old_pos - 1
        if new_pos > 7:
            return      # we are already at position 7 (right border), cannot move further
        if new_pos < 0:
            return      # we are already at position 0 (left border), cannot move further

        self._pixels = self._getCrossPixels(new_pos)        # set the new covered pixels
        self._crossPos = new_pos        # set new position
        self._displayCross()            # paints cross (at the new position self._crossPos)
        self._restoreCrossPixels(old_pixels, old_pos)       # this restores the pixels covered by the old cross

    def get_cross_pos(self) -> int:
        """
        Get the position of the cross
        :return:    the column number (0..7)
        """
        return self._crossPos


# ------- test code ------------------------------

_test_colors = [(204, 204, 204), pixelUtils.a, pixelUtils.b, pixelUtils.red,
                pixelUtils.g, pixelUtils.h, pixelUtils.green, pixelUtils.blue]


def _test_cut(col: int):
    logger.debug('! cut wire at position {} !'.format(col))


def _horizontal_stripes(_s: SenseHat):
    _s.low_light = False
    for y in range(8):
        color = _test_colors[y]
        for x in range(8):
            _s.set_pixel(x, y, color)


def _vertical_stripes(_s: SenseHat):
    _s.low_light = False
    for x in range(8):
        color = _test_colors[x]
        for y in range(8):
            _s.set_pixel(x, y, color)


def _test_all_positions(_s: SenseHat, meter: WiresInclinationMeter):
    import time
    for col in range(8):
        time.sleep(1)
        meter.show_cross(col)
        time.sleep(0.5)
        meter.hide_cross()
    time.sleep(1)


def _test_with_and_without_pos(_s: SenseHat):
    wirePos = [1, 3, 5]
    meter = WiresInclinationMeter(_s, wirePos, _test_cut)
    import time
    for col in range(8):
        time.sleep(1)
        meter.show_cross(col)
        time.sleep(0.5)
        meter.hide_cross()
        time.sleep(0.5)
        meter.show_cross()
        time.sleep(0.5)
        meter.hide_cross()
    time.sleep(1)


def _test_show_hide_cross(_s: SenseHat):
    wirePos = [1, 3, 5]
    meter = WiresInclinationMeter(_s, wirePos, _test_cut)
    _horizontal_stripes(_s)
    _test_all_positions(_s, meter)
    _vertical_stripes(_s)
    _test_all_positions(_s, meter)


def _test_move_cross(_s: SenseHat):
    wirePos = [0, 2, 4, 6]
    import time
    meter = WiresInclinationMeter(_s, wirePos, _test_cut)
    _vertical_stripes(_s)
    meter.show_cross(1)
    for i in range(8):  # try to move beyond right border
        meter._move_cross(True)
        time.sleep(.7)
    for i in range(8):  # try to move beyond left border
        meter._move_cross(False)
        time.sleep(.7)
    for i in range(8):  # "dance"
        meter._move_cross(True)
        time.sleep(.5)
        meter._move_cross(False)
        time.sleep(.5)
        meter._move_cross(True)
        time.sleep(.7)


def _test_withInclination(_s: SenseHat):
    wirePos = [0, 2, 4, 6]
    meter = WiresInclinationMeter(_s, wirePos, _test_cut)
    _vertical_stripes(_s)
    meter.show_cross(3)
    print('meter initialized: ', meter)

    import time
    rep = 200
    print('in test, will measure and update inclination {}x'.format(rep))
    for i in range(rep):
        meter.update()
        time.sleep(0.2)
    print('test finished')


def _test():
    _s = SenseHat()
    _s.low_light = False

    # _test_show_hide_cross(_s)
    # _test_with_and_without_pos(_s)
    # _test_move_cross(_s)
    _test_withInclination(_s)


if __name__ == "__main__":      # to start this: `python3 -m bombgame.simplewires.wiresInclinationMeter`
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    _test()
