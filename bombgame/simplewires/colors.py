from enum import IntEnum, unique

@unique
class WireColor(IntEnum):
    WHITE = 0
    YELLOW = 1
    RED = 2
    BLUE = 3
    BLACK = 4
