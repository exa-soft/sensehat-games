import logging
from bombgame.core.common import SenseHat
from bombgame.core.exceptions import ArgumentError
from bombgame.core.game import GameWindow
from bombgame.simplewires.wiresOnInclination import WiresOnInclination

sense = SenseHat()
logger = logging.getLogger('wires.window')


class WiresOIWindow(GameWindow):

    woi = None      # WiresOnInclination object
    _serial = ''    # serial number

    # Subclasses must overwrite:

    def init_game(self, **moreArgs):
        serial = moreArgs['serial']
        if serial is None:
            raise ArgumentError(serial, 'serial number expected')
        self._serial = serial
        logger.debug('WiresOIWindow initialized (serial number {})'.format(serial))

    def get_name(self):
        return 'Wires on inclination ({})'.format(self.woi.game_id)

    def start_game(self):
        self.woi = WiresOnInclination(self._serial)
        logger.debug('WiresOIWindow started (init WiresOnInclination)')
        self.woi.restart()

    def play(self):
        self.woi.listen()

    def is_solved(self):
        if self.woi is None:
            return False
        else:
            return self.woi._solved

    # Optionally, subclasses can overwrite:

    def pause_game(self):
        """Stop listening (stop measuring the inclination).
        """
        self.woi._listening = False
        logger.info('pause_game: stopped listening. cut is {}'
                    .format(self.woi._cut))

    def continue_game(self):
        """Restart the same round again, but only if the game has not been solved"""
        if not self.is_solved():
            self.woi.nextTry()

    def get_screen(self):
        logger.debug('woiw.get_screen, self.is_solved={}'.format(self.is_solved()))
        if self.is_solved():
            return self.get_solved_screen()
        else:
            logger.debug('woiw.get_screen, not solved')
            return GameWindow.get_screen(self)

    def get_solved_screen(self):
        # this method must only be called when self.is_solved()
        return self.woi.solved_screen

    def get_border_color(self):
        """Return a nice yellow for the border"""
        return 200, 200, 200        # grey
