import logging
from time import sleep
from typing import List

from bombgame.core.common import SenseHat
from bombgame.core.exceptions import ArgumentError
from bombgame.output.displayUtils import Color
from .simplewires import WireColor

logger = logging.getLogger('wires.display')


# TODO define color codes for each color in Colors
cross1 = (0, 204, 0)     # green
cross2 = (153, 255, 153)     # light green
color_off = (102, 102, 102)  # background
red = (204, 0, 0)
yellow = (255, 255, 0)  # yellow
white = (255, 255, 255)
blue = (0, 51, 255)
black = (0, 0, 0)


colormap = {
    WireColor.WHITE: white,
    WireColor.YELLOW: yellow,
    WireColor.RED: red,
    WireColor.BLUE: blue,
    WireColor.BLACK: black
}


def display_wires(sense: SenseHat, wires: List[WireColor], cut: List[bool]) -> None:
    """
    Display the wires across the display on the sensehat
    :param sense:   SenseHAT
    :param wires:   the wires to display (List with 3 or 4 elements)
    :param cut:     if the wires are cut (List with 3 or 4 elements, same size as wires)
    """

    sense.clear(color_off)
    size = len(wires)
    if size < 3 or size > 4:
        raise ArgumentError(size, "size for displaying wires must be 3 or 4")
    if  len(cut) != size:
        raise ArgumentError(size, "size of wires and size of cut must be same")

    for i in range(0, len(wires)):
        wireCol = colormap[wires[i]]
        display_wire(sense, size, i, wireCol, cut[i])


def pos_for_column(size: int, column: int) -> int:
    """
    Calculate the SenseHat column for the given size (number of wires) and wire position.
    :param size:    game size (3 or 4, ArgumentError if otherwise)
    :param column:  wire column (0..7)
    :return:        the position position (0..2 or 0..3) for the wire
    """
    if size < 3 or size > 4:
        raise ArgumentError(size, "pos_for_column: size must be 3 or 4")

    # this is the inverse function of the one in _column_for_pos
    raw = column + size - 4
    if raw % 2 == 1:
        raise ArgumentError(size, "pos_for_column: size {} has no wire at column {}".format(size, column))
    return (column + size - 4) // 2


def _column_for_pos(size: int, pos: int) -> int:
    """
    Calculate the SenseHat column for the given size (number of wires) and wire position.
    :param size:    game size (3 or 4, but will not be tested)
    :param pos:     wire position (0..2 or 0..3)
    :return:        the column for the wire (0..7)
    """
    # columns are
    # - for 3 wires: 1, 3, 5
    # - for 4 wires: 0, 2, 4, 6
    # so as a formula:
    return pos * 2 + 4 - size


def display_wire(sense: SenseHat, size: int, pos: int, color: Color, is_cut: bool) -> None:
    """
    Display one wire at its position
    :param sense:   SenseHat
    :param size:    number of total wires (3 or 4)
    :param pos:     position of the wire (0..2 or 0..3)
    :param color:   color of the wire
    :param is_cut   true if the wire is cut
    """
    if size < 3 or size > 4:
        raise ArgumentError(size, "size for displaying wires must be 3 or 4")
    if pos < 0 or pos >= size:
        raise ArgumentError(size, "pos must be between 0 and size - 1")
    col = _column_for_pos(size, pos)
    logger.debug("display_wire {} (cut: {})".format(pos, is_cut))
    midColor = color_off if is_cut else color
    for i in range(0, 3):
        sense.set_pixel(col, i, color)
    sense.set_pixel(col, 3, midColor)
    sense.set_pixel(col, 4, midColor)
    for i in range(5, 8):
        sense.set_pixel(col, i, color)


def get_columns_for_size(size: int) -> List[int]:
    """
    Returns the columns for the wires, depending if we have 3 or 34 wires.
    :param size:    number of wires (3 or 4)
    :return:        indices for the wires
    """
    # could be done by looping over _column_for_pos, but hardcoded is easier
    if size < 3 or size > 4:
        raise ArgumentError(size, "size for displaying wires must be 3 or 4")
    if size == 3:
        return [1, 3, 5]
    else:
        return [0, 2, 4, 6]


def _show_cross(sense: SenseHat, col: int, color: Color) -> None:
    """
    Displays a cross in the given column (0..7).
    :param sense:   SenseHAT
    :param col      column of the senseHAT display where to display center of the cross
    :param active   true to display the cross as active (different color, only during cutting
    """
    if col < 0 or col > 7:
        raise ArgumentError(col, "col for displaying cross must be between 0 and 7")

    # sense.set_pixel(col, 1, color)
    sense.set_pixel(col, 2, color)
    sense.set_pixel(col, 4, color)
    # sense.set_pixel(col, 5, color)
    if col >= 1:
        sense.set_pixel(col - 1 , 3, color)
        # if col >= 2:
            # sense.set_pixel(col - 2, 3, color)
    if col <= 6:
        sense.set_pixel(col + 1 , 3, color)
        # if col <= 5:
            # sense.set_pixel(col + 2, 3, color)


def show_cross(sense: SenseHat, col: int, active: bool) -> None:
    """
    Displays a cross in the given column (0..7).
    :param sense:   SenseHATco
    :param col      columnt of the senseHAT display where to display center of the cross
    :param active   true to display the cross as active (different color, only during cut:      position to cut (0..2 or 0..3)
    """
    if col < 0 or col > 7:
        raise ArgumentError(col, "column for displaying cross must be between 0 and 7")
    color = cross2 if active else cross1
    _show_cross(sense, col, color)


def clear_cross(sense: SenseHat, col: int) -> None:
    """
    Clears the cross pixels to the background color. Note that after this, the wires have to be dranw again
    :param sense:   SenseHATco
    :param col      column of the senseHAT display where to display center of the cross
    """
    if col < 0 or col > 7:
        raise ArgumentError(col, "column for displaying cross must be between 0 and 7")
    _show_cross(sense, col, color_off)


# test code ###########################################

_s = SenseHat()


# noinspection PyPep8Naming
def _testWire_separate():
    _s.clear()
    display_wire(_s, 3, 0, red, False)


def _testWire_array3():
    wires = [WireColor.RED, WireColor.WHITE, WireColor.BLUE]
    cut = [False, True, False]
    display_wires(_s, wires, cut)


def _testWire_array4():
    wires = [WireColor.YELLOW, WireColor.BLACK, WireColor.WHITE, WireColor.BLUE]
    cut = [False, True, False, True]
    display_wires(_s, wires, cut)

def _testCross():
    for i in range(0, 8):
        sleep(0.5)
        _s.clear(color_off)
        show_cross(_s, i, False)
    for i in range(0, 8):
        sleep(0.5)
        _s.clear(color_off)
        show_cross(_s, i, True)


if __name__ == "__main__":       # to start this: `python3 -m bombgame.simplewires.displayWires`
    _s.clear()
    _testCross()
    sleep(1)
    _testWire_separate()
    sleep(2)
    _testWire_array3()
    sleep(2)
    _testWire_array4()
    sleep(2)
    show_cross(_s, 4, False)
