import logging

from .simplewires import SimpleWires

logger = logging.getLogger('wires.console')

class WiresOnConsole(SimpleWires):

    def onRestart(self) -> None:
        """Reset display on restart"""
        print()
        print("neues Spiel. Seriennummer {}".format(self._serial))

    def _displayUncutWire(self, index: int):
        color = self._wires[index]
        logger.debug('- wire {}: {}'.format(index + 1, color))
        print('- Draht ', index + 1, ' ist ', color)

    def _displayCutWire(self, index: int):
        color = self._wires[index]
        logger.debug('- cut {}: {}'.format(index + 1, color))
        print('- Draht ', index, ' ist ', color, ' und zerschnitten')

    def displayCuttingWire(self, index: int):
        print('- Draht ', index, ' wird zerschnitten...')

    def displayCuttingInvalid(self, index: int):
        print(
            "- Draht {} kann man nicht schneiden (schon geschnitten oder ist kein Draht), versuch nochmals mit anderer Nummer"
                .format(index)
        )

    def nextTry(self):
        position = int(input("Welchen Draht schneiden?  (1, 2, ...)  "))
        print ("Du willst Draht {} schneiden...".format(position))
        self.cutWire(position - 1)

    def displayWrongWire(self):
        print("-----!!! upps, das war falsch !!!-----------")

    def displayGameSolved(self):
        print("------- >>> du bist super! <<<-------")

    def wrongLost(self):
        print("-----!!! GAME OVER !!!-----------")

    def __str__(self):
        return 'WiresOnConsole: serial={s._serial}, wires={s._size}, temp solution = {s._solution})'\
            .format(s=self)

def _test():
    woc = WiresOnConsole()
    woc.restart()

if __name__ == "__main__":      # to start this: `python3 -m bombgame.simplewires.wiresOnConsole`
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=f)
    logger.setLevel(logging.DEBUG)
    # logger.setLevel(logging.INFO)
    # logger.addHandler(logging.StreamHandler())
    _test()
