"""
A game to test the GameWindowGrid.
Displays a countdown of numbers in its "window", in changing colors.
Stops the countdown when the game is not running.
"""

from ..core.common import SenseHat
import logging
from time import sleep
from math import ceil
from ..core.game import GameWindow
from ..core.exceptions import ArgumentError
from threading import Condition
from colorsys import hsv_to_rgb


_s = SenseHat()
logger = logging.getLogger('dummy.countdown')


def map_tuple_gen(func, tup):
    """Applies func to each element of tup and returns a new tuple."""
    return tuple(func(itup) for itup in tup)


def scale(v):
    return int(v * 255)


def scale_tuple(t):
    return map_tuple_gen(scale, t)


class CountdownGame (GameWindow):

    id_letter = ''
    max_value = 0
    _count = 0.0

    def init_game(self, **moreArgs):
        """Init game: expects parameter 'seconds'."""
        logger.debug('new CountdownGame: moreArgs is {}'.format(moreArgs))
        m = moreArgs['seconds']
        if m < 1:
            raise ArgumentError(m, 'max_value must be > 0 and < 10')

        self.id_letter = self.game_id[0]
        self.max_value = min(9, m)
        logger.debug('init game {} - set _count to {}'.format(self.game_id, self.max_value))
        self._count = float(self.max_value)

    def get_name(self):
        return self.game_id

    def start_game(self):
        logger.debug('start game {} with value {}'.format(self.game_id, self.max_value))

    def play(self):
        # for the color, use the fractal part of the counter and multiply by 0.6
        # (so it starts at blue and goes down to red)
        logger.debug('enter play game {}'.format(self.game_id))
        if self.is_solved():
            _s.show_letter('0', text_colour=(248, 0, 0), back_colour=(120, 0, 0))
        elif self.running:
            sleep(0.1)
            self._count -= 0.2
            if self._count <= 0:
                color_unscaled = hsv_to_rgb(0.0, 1.0, 1.0)  # red
                # hsv_to_rgb returns 0..1 floats; convert to int in the range 0..255
                color = scale_tuple(color_unscaled)
                _s.show_letter('0', text_colour=color, back_colour=[0, 0, 0])
            else:
                fractals = (self._count % 1.0)
                hue = fractals * 0.6
                # Convert the hue to RGB values
                color_unscaled = hsv_to_rgb(hue, 1.0, 1.0)
                # hsv_to_rgb returns 0..1 floats; convert to int in the range 0..255
                color = scale_tuple(color_unscaled)
                # Update the display
                letter = str(ceil(self._count))[0]
                logger.debug('count is {}, letter is {}'.format(self._count, letter))
                _s.show_letter(letter, text_colour=color, back_colour=[0, 0, 0])

    def is_solved(self):
        return self._count <= 0


def _test():
    _s.low_light = False
    # global gm
    cond = Condition()
    gm = CountdownGame('A-Game', seconds=4)
    print('game is {}'.format(gm))
    gm.resume_game()
    print('game is {}'.format(gm))
    sleep(2)

    gm.leave_game()
    pixlist = [(120, 160, 80)] * 64
    _s.set_pixels(pixlist)
    sleep(2)

    gm.resume_game()
    sleep(2)

    # logger.info('This is a very basic test. For better tests of this '
    #             + 'class, run counter._test()')


if __name__ == "__main__":  # to start this: `python3 -m bombgame.dummy.countdownGame`
    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-15s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.INFO, format=f)
    logger.setLevel(logging.DEBUG)
    logger2 = logging.getLogger('core.game')
    logger2.setLevel(logging.DEBUG)
    # create console handler (could change log level)
    # ch = logging.StreamHandler()
    # ch.setFormatter(logging.Formatter(f))
    # ch.setLevel(logging.ERROR)
    # logger.addHandler(ch)
    # logger2.addHandler(ch)
    _test()
