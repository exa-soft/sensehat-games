"""
This is inspired from https://pythonhosted.org/sense-hat/api/#joystick
"""


import logging
import time
from threading import Condition
from signal import pause, signal, SIGINT
# from sense_hat import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED, \
#     DIRECTION_DOWN, DIRECTION_UP, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_MIDDLE
# from sense_emu import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED, \
#     DIRECTION_DOWN, DIRECTION_UP, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_MIDDLE
from ..core.common import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED, \
    DIRECTION_DOWN, DIRECTION_UP, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_MIDDLE
from ..output import fieldScroller
from ..core.exceptions import ArgumentError
from ..core.game import GameWindow

logger = logging.getLogger('dummy.pixel2')
sense = SenseHat()

_syncCond = Condition()

width = 3
"""width: width of the games grid, defaults to 3"""

height = 2
"""height: height of the games grid"""

posX = 0
"""X-position of the current game in the grid."""

posY = 0
"""Y-position of the current game in the grid."""

scrollSpeed = 0.2
"""Speed of scrolling. Number is wait time between steps (higher means
slower).
"""

_games = [[None for x in range(width)] for y in range(height)]


def exit_handler(sig, frame):
    """
    Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    sense.clear()
    raise SystemExit


# Set up signal handler to catch Ctrl-c
signal(SIGINT, exit_handler)


def set_game(x, y, game):
    """Set a game to a position (x, y) in the game grid."""
    if x >= width:
        raise ArgumentError(x, "x must be 0 < x <= width")
    if y >= height:
        raise ArgumentError(y, "y must be 0 < y <= height")
    _games[x][y] = game


def get_synccond():
    return _syncCond


sense = SenseHat()
_syncCond = Condition()


# noinspection PyPep8Naming
def start():
    """Start the overall game (this starts the current game).

    Start: after all the games have been initialized, call this
    method to start the first game (the one at position posX, posY).
    """
    #     sense.stick.direction_up = go_up
    #     sense.stick.direction_down = go_down

    nextGame = get_game(posX, posY)
    nextGame.resume_game()
    # play()


def play():  # TODO probably not necessary
    """The main game loop.

    It will listen for and handle joystick events (only up/down and
    left/right) and then call play_1step() of the current game.
    """
    logger.info('play not yet implemented! ')

    # while True:
    #     for event in​ sense.stick.get_events():
    #         print( event.direction )
    #         print( event.action )


def get_game(x, y):
    """ Return the game at the position x, y."""
    if x >= width:
        raise ArgumentError(x, "x must be 0 < x <= width")
    if y >= height:
        raise ArgumentError(y, "y must be 0 < y <= height")

    return _games[x][y]


# noinspection PyPep8Naming
def go_up():
    """Move to the game above the current one, if possible.

    If possible, moves the current position up and returns True.
    Returns False if the current position is already in the topmost row.
    """
    global posX, posY
    logger.info('go_up requested from current pos {}/{}'.format(posX, posY))
    thisGame = get_game(posX, posY)
    oldCol = thisGame.get_border_color()

    if posY == 0:
        logger.info('go_up: no game above')
        fieldScroller.try_scroll_up(oldCol, scrollSpeed)

    else:
        thisGame.leave_game()
        posY -= 1
        nextGame = get_game(posX, posY)
        logger.info('go_up: next game (at pos {}/{}): {}'.format(posX, posY, nextGame.gameId))
        data = nextGame.get_screen()
        newCol = nextGame.get_border_color()
        fieldScroller.scroll_up(data, [oldCol, newCol], scrollSpeed)
        nextGame.resume_game()


# noinspection PyPep8Naming
def go_down():
    """Move to the game below the current one, if possible.

    If possible, moves the current position down and returns True.
    Returns False if the current position is already in the last row.
    """
    global posX, posY
    logger.info('go_down requested from current pos {}/{}'.format(posX, posY))
    thisGame = get_game(posX, posY)
    oldCol = thisGame.get_border_color()

    if posY == height - 1:
        logger.info('go_down: no game below')
        fieldScroller.try_scroll_down(oldCol, scrollSpeed)

    else:
        thisGame.leave_game()
        posY += 1
        nextGame = get_game(posX, posY)
        logger.info('go_down: next game (at pos {}/{}): {}'.format(posX, posY, nextGame.gameId))
        data = nextGame.get_screen()
        newCol = nextGame.get_border_color()
        fieldScroller.scroll_down(data, [oldCol, newCol], scrollSpeed)
        nextGame.resume_game()


# TODO go_left, go_right


def __str__():
    return 'GameWindowGrid DUMMY with {}x{} games, position {}/{}'.format(width, height, posX, posY)


# noinspection PyPep8Naming
def _test2Games_noJoystick():
    sense.clear()

    logger.debug('before definition of stick handlers...')
    sense.stick.direction_up = go_up
    sense.stick.direction_down = go_down
    sense.stick.direction_left = handle_event_test
    sense.stick.direction_right = handle_event_test
    sense.stick.direction_any = handle_event_test
    logger.debug('...after definition of stick handlers')

    global width
    width = 1
    game1 = GameWindow("A", get_synccond())
    game2 = GameWindow("B", get_synccond())
    print(__str__())
    set_game(0, 0, game1)
    set_game(0, 1, game2)
    x = 0
    for y in range(2):
        g = get_game(x, y)
        print('game at {}/{}: {}'.format(x, y, g))

    start()
    print('game1 running')
    time.sleep(1)
    print('go down...')
    go_down()
    time.sleep(2)
    print('go down...')
    go_down()
    time.sleep(2)
    print('go up...')
    go_up()
    time.sleep(2)
    print('go up...')
    go_up()
    time.sleep(2)
    pause()


# noinspection PyPep8Naming
def handle_event_test(_event):
    logger.debug("joystick was {} {}".format(_event.action, _event.direction))
    # Check if the joystick was pressed or held (but not released)
    if _event.action != ACTION_RELEASED:
        # Check which direction
        if _event.direction == DIRECTION_UP:
            sense.show_letter("U")  # Up arrow
        elif _event.direction == DIRECTION_DOWN:
            sense.show_letter("D")  # Down arrow
        elif _event.direction == DIRECTION_LEFT:
            sense.show_letter("L")  # Left arrow
        elif _event.direction == DIRECTION_RIGHT:
            sense.show_letter("R")  # Right arrow
        elif _event.direction == DIRECTION_MIDDLE:
            sense.show_letter("M")  # Enter key

        # Wait a while and then clear the screen
        time.sleep(0.5)
        sense.clear()
    else:
        print("other event")

    pass


pix_x = 3
pix_y = 3


def clamp(value, min_value=0, max_value=7):
    return min(max_value, max(min_value, value))


def move_dot(event):
    global pix_x, pix_y
    logger.debug("joystick was {} {}".format(event.action, event.direction))

    global posX, posY
    logger.info('go_up requested from current pos {}/{}'.format(posX, posY))
    thisGame = get_game(posX, posY)
    # oldCol = thisGame.get_border_color()

    logger.info('have game: {}'.format(thisGame))

    if event.action in (ACTION_PRESSED, ACTION_HELD):
        pix_x = clamp(pix_x + {
            'left': -1,
            'right': 1,
            }.get(event.direction, 0))
        pix_y = clamp(pix_y + {
            'up': -1,
            'down': 1,
            }.get(event.direction, 0))


def refresh():
    sense.clear()
    sense.set_pixel(pix_x, pix_y, 255, 255, 255)


def play():
    msg = "{} is my name".format('xyz')
    sense.show_message(msg, scroll_speed=0.05, text_colour=[255, 255, 0], back_colour=[0, 0, 0])


def preface():
    logger.info('game1 running')
    logger.debug('play: before')
    play()
    logger.debug('play: after')




f = '%(relativeCreated)6d %(threadName)-12s %(name)-15s %(levelname)-6s %(message)s'
logging.basicConfig(level=logging.INFO, format=f)
logger.setLevel(logging.DEBUG)


logger.debug('before definition of stick handlers')
sense.stick.direction_up = move_dot
# sense.stick.direction_up = go_up
sense.stick.direction_down = move_dot
# sense.stick.direction_down = go_down

sense.stick.direction_left = move_dot
sense.stick.direction_right = move_dot
# sense.stick.direction_left = pushed_left
# sense.stick.direction_right = pushed_right
sense.stick.direction_any = refresh
logger.debug('before definition of stick handlers')


width = 1
# game1 = GameWindow("A", None)
# game2 = GameWindow("B", None)
# game1 = GameWindow("A", get_synccond())
# game2 = GameWindow("B", get_synccond())
"""
print(__str__())
set_game(0, 0, game1)
set_game(0, 1, game2)
x = 0
for y in range(2):
    g = get_game(x, y)
    print('game at {}/{}: {}'.format(x, y, g))
"""

def _test_y():
    from ..dummy.dummyGame import DummyGame
    from signal import pause
    sense.clear()

    global width
    width = 2
    game1 = DummyGame("A", get_synccond())
    game2 = DummyGame("B", get_synccond())
    game3 = DummyGame("C", get_synccond())
    game4 = DummyGame("D", get_synccond())
    print(__str__())
    """
    set_game(0, 0, game1)
    set_game(1, 0, game2)
    set_game(0, 1, game3)
    set_game(1, 1, game4)
    # start()

    for x in range(2):
        for y in range(2):
            g = get_game(x, y)
            logger.debug('game at {}/{}: {}'.format(x, y, g))
    """
    pause()

    # while True:
    #     logger.debug('wait for event')
    #     # event = sense.stick.wait_for_event(emptybuffer=True)
    #     event = sense.stick.wait_for_event()    # seems not to work on emulator!
    #     handle_event_test(event)
    #     time.sleep(0.1)


sense.clear()

preface()
refresh()
pause()

# to start this: `python3 -m bombgame.dummy.navigatePixelTest2`
