"""
This module contains a window with ...
"""

__author__ = 'Edith Birrer'
__version__ = '1.0'


import logging
from ..core.common import SenseHat
# from . import exceptions
# from time import sleep
from ..core.game import GameWindow

logger = logging.getLogger('myModule.myNewGame')
sense = SenseHat()


class SomeWindow(GameWindow):

    # Subclasses must overwrite:

    def init_game(self):
        pass

    def get_name(self):
        pass

    def start_game(self):
        pass

    def play(self):
        pass

    def is_solved(self):
        pass

    # Optionally, subclasses can overwrite:

    def pause_game(self):
        pass

    def continue_game(self):
        pass

    def get_screen(self):
        pass

    def get_border_color(self):
        pass
