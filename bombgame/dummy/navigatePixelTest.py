"""
This is from https://pythonhosted.org/sense-hat/api/#joystick
"""

# from sense_hat import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED
# from sense_emu import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED
from ..core.common import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED
from signal import pause, signal, SIGINT


def exit_handler(sig, frame):
    """
    Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    sense.clear()
    raise SystemExit


# Set up signal handler to catch Ctrl-c
signal(SIGINT, exit_handler)


sense = SenseHat()
x = 3
y = 3


def clamp(value, min_value=0, max_value=7):
    return min(max_value, max(min_value, value))


def move_dot(event):
    global x, y
    if event.action in (ACTION_PRESSED, ACTION_HELD):
        x = clamp(x + {
            'left': -1,
            'right': 1,
            }.get(event.direction, 0))
        y = clamp(y + {
            'up': -1,
            'down': 1,
            }.get(event.direction, 0))


# use the above (nice to do similar things for all directions) or the
# handlers below (better if the code for the directions is more different)

def pushed_up(event):
    global y
    if event.action != ACTION_RELEASED:
        y = clamp(y - 1)


def pushed_down(event):
    global y
    if event.action != ACTION_RELEASED:
        y = clamp(y + 1)


def pushed_left(event):
    global x
    if event.action != ACTION_RELEASED:
        x = clamp(x - 1)


def pushed_right(event):
    global x
    if event.action != ACTION_RELEASED:
        x = clamp(x + 1)


def refresh():
    sense.clear()
    sense.set_pixel(x, y, 255, 255, 255)


sense.stick.direction_up = move_dot
sense.stick.direction_down = move_dot
sense.stick.direction_left = move_dot
sense.stick.direction_right = move_dot
# sense.stick.direction_up = pushed_up
# sense.stick.direction_down = pushed_down
# sense.stick.direction_left = pushed_left
# sense.stick.direction_right = pushed_right
sense.stick.direction_any = refresh
refresh()
pause()

# to start this: `python3 -m bombgame.dummy.navigatePixelTest`
