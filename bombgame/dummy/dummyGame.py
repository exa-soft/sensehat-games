"""
Dummy game in a "window"
"""

from ..core.common import SenseHat
import logging
import time
from ..core.game import GameWindow

sense = SenseHat()
logger = logging.getLogger('dummy.game')


class DummyGame (GameWindow):
    """
    A "game" that only displays its first letter and then its name.
    Will set itself to solved when it has been left and then continued once.
    """

    name = ''   # derived from the game_id
    letter = ''
    resume_counter = 0

    def init_game(self, **moreArgs):
        """Init game: expects no parameters in moreArgs."""
        self.name = self.game_id + "!"
        self.letter = self.game_id[0]
        self.resume_counter = 0
        logger.debug('_init_game, name is {}'.format(self.name))

    def get_name(self):
        """ Return the name of the game"""
        return self.name

    def get_border_color(self):
        """ Color for the border (used when scrolling), can serve to 
        easier identify the game"""
        return 204, 204, 120

    def play(self):
        """Play the game: one execution inside the main game loop."""
        msg = "{1}: 1st letter of my name '{0}'...".format(self.name, self.letter)
        sense.show_message(msg, scroll_speed=0.05, text_colour=[255, 255, 0], back_colour=[0, 0, 0])

    def start_game(self):
        """Start the game. Will be called when resume_game() is 
        called for the first time."""
        sense.show_letter(self.letter, text_colour=[200, 200, 0], back_colour=[0, 0, 0])
        time.sleep(1.0)

    def continue_game(self):
        """Continue the game. Will be called when resume_game() is 
        called not for the first time, but the game is not yet solved."""
        sense.show_letter(self.letter, text_colour=[200, 200, 0], back_colour=[0, 80, 0])
        self.resume_counter += 1    # this will cause the game to be "solved"...
        logger.debug('----------- set to solved -----------')
        time.sleep(0.5)

    def is_solved(self):
        """Return true if the game is solved"""
        solved = self.resume_counter > 0
        return solved


def _test_resume():
    from threading import Condition

    game = DummyGame('testgame')
    game.set_synccondition(Condition())
    game.resume_game()
    time.sleep(2)
    game.resume_game()
    time.sleep(2)
    game.resume_game()


def _test_leave():
    from threading import Condition

    g2 = DummyGame('more')
    g2.set_synccondition(Condition())
    g2.resume_game()
    time.sleep(1)
    g2.resume_game()
    time.sleep(1)
    print('before leave')
    g2.leave_game()
    time.sleep(1)

    pixlist = [[120, 160, 80]] * 64
    print('pixlist is {}'.format(pixlist))
    sense.set_pixels(pixlist)
    time.sleep(2)
    scr = g2.get_screen()
    print('screen(len{}) is {}'.format(len(scr), scr))
    g2.resume_game()
    time.sleep(2)
    print('restoring screen')
    sense.set_pixels(scr)
   

if __name__ == "__main__":      # to start this: `python3 -m dummy.dummyGame`
    logger.setLevel(logging.DEBUG)
    # _test_resume()
    _test_leave()
