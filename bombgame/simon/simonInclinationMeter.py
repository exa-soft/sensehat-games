from ..core.common import SenseHat
from typing import Callable
from ..accele_input.inclinationMeter import InclinationMeter, Direction, Steepness, Inclination
from ..accele_input.inclinationMeter import get_inclination
from .displayUtils import display_full, display_quadrant, Brightness
import logging

# LATER we could save and display both the left/right and backward/forward direction, but only care about the bigger one

logger = logging.getLogger('simon.meter')


class SimonInclinationMeter(InclinationMeter):
    """
    Class to wrap the analysis and display of the inclination of a SenseHAT
    for the SimonSays game.
    Values over 30° can raise a callback (a method that takes a SenseHAT and a direction as parameter).
    """

    #                   # with typing:
    _full_sent = False  # _full_sent: ClassVar[bool]
    # _full_sent is set when full has been reached and will be reset only when a different inclination level has
    # been detected. This helps to avoid calling the callback all the time while the sensors detect full inclination.
    _callback = None    # _callback: ClassVar[Callable[[SenseHat, Direction], None]]

    # TODO remove sense parameter in callback (not used), but test it first)
    def __init__(self, sensehat: SenseHat, call_for_dir: Callable[[SenseHat, Direction], None]) -> None:
        """
        Init the inclination meter for the given SenseHAT.
        :param sensehat: SenseHAT
        :param call_for_dir: function to be called when the full inclination for a direction has been reached.
            (function must take a SenseHAT and a Direction)
        """
        super().__init__(sensehat)
        self._callback = call_for_dir
        logger.info('SimonInclinationMeter initialized')

    def __str__(self) -> str:
        return 'SimonInclinationMeter (current: {sf._cur}, previous: {sf._prev})'.format(sf=self)

    def clear(self) -> None:
        """
        Clear the screen and reset the internal variables.
        """
        self._prev = None
        self._cur = (Direction.NONE, Steepness.NEUTRAL)
        self._full_sent = False
        self._sense.clear()

    def display(self, incli: Inclination, brightness: Brightness) -> None:
        """
        Displays a bar for the current inclination (direction, steepness) in the given brightness.
        :param incli:   the inclination. If None is given, the method does nothing.
        :param brightness:  brightness to use for display
        """

        if incli is None:
            return
        steep = incli[1]     # steep: Steepness
        if steep >= Steepness.LOW:  # ignore neutral steepness
            display_quadrant(self._sense, incli[0], steep, brightness)

    def update(self) -> None:
        """
        Measures the inclination of the SenseHAT and if the direction or steepness level has changed, the display
        is updated accordingly. This also takes into account the previous situation (will be dimmed).
        """

        incli = get_inclination(self._sense)   # incli: Inclination

        if incli[1] == Steepness.HIGH:
            # for steep inclinations, the whole square is displayed, so we do not care about displaying the previous
            display_full(self._sense, incli[0], Brightness.BRIGHT)
        elif self._cur[1] == Steepness.HIGH:
            logger.debug('- was full before: {0}'.format(self._cur))
            # if previous was full and current is not, clear the full square and display the new situation
            display_full(self._sense, incli[0], Brightness.OFF)
            logger.debug('- is not full now: {0}'.format(incli))
            self.display(incli, Brightness.BRIGHT)  # display new inclination with bright light
            # mark that we have left the "fully inclined" situation
            self._full_sent = False
        else:
            # display/clear bars for previous status because we have not reached full inclination
            logger.debug('- is not full now nor before: {0}'.format(incli))
            self.display(incli, Brightness.BRIGHT)  # display new inclination with bright light
            if incli != self._cur:
                self.display(self._cur, Brightness.WEAK)  # if different, display previous with weak light
            if self._cur != self._prev:
                self.display(self._prev, Brightness.OFF)  # if different, clear old previous inclination
            # mark that we have left the "fully inclined" situation
            self._full_sent = False

        self._prev = self._cur  # anyway, assign fields accordingly
        self._cur = incli

        # for high steepness and not already done: call callback
        if not self._full_sent and incli[1] == Steepness.HIGH:
            self._full_sent = True
            self._callback(self._sense, incli[0])

# TODO remove sense parameter in callback
def _test_full(s: SenseHat, d: Direction):
    logger.debug('reached full inclination in direction {0} on {1}'.format(d, s))


def _test():
    import time
    _s = SenseHat()
    _s.low_light = False

    meter = SimonInclinationMeter(_s, _test_full)
    print('meter initialized: ', meter)

    rep = 200
    print('in test, will measure and display inclination {}x'.format(rep))
    for i in range(rep):
        meter.update()
        # time.sleep(0.2)
    print('test finished')


if __name__ == "__main__":      # to start this: `python3 -m bombgame.simon.simonInclinationMeter`
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    _test()
