import logging
from bombgame.core.common import SenseHat
from bombgame.core.exceptions import ArgumentError
from bombgame.core.game import GameWindow
from bombgame.simon.simonOnInclination import SimonOnInclination

sense = SenseHat()
logger = logging.getLogger('simon.window')


class SimonOIWindow(GameWindow):

    soi = None      # SimonOnInclination object
    game_length = 0

    # Subclasses must overwrite:

    def init_game(self, **moreArgs):
        len = moreArgs['len']
        if len > 20:
            raise ArgumentError(len, 'game length must not be > 20')
        self.game_length = len
        logger.debug('SimonOIWindow initialized')

    def get_name(self):
        return 'simon on inclination ({})'.format(self.soi.game_id)

    def start_game(self):
        self.soi = SimonOnInclination(self.game_length)
        logger.debug('SimonOIWindow started (init SimonOnInclination)')
        self.soi.restart()

    def play(self):
        self.soi.listen()

    def is_solved(self):
        if self.soi is None:
            return False
        else:
            return self.soi.is_solved()

    # Optionally, subclasses can overwrite:

    def pause_game(self):
        """Stop listening (stop measuring the inclination).
        """
        self.soi._listening = False
        logger.info('pause_game: stopped listening. cur_len is {} of {}'
                     .format(self.soi._curLen, self.soi._size))

    def continue_game(self):
        """Restart the same round again, but only if the game has not been solved"""
        if not self.is_solved():
            self.soi.repeatRound()

    def get_screen(self):
        logger.debug('soiw.get_screen, self.is_solved={}'.format(self.is_solved()))
        if self.is_solved():
            return self.get_solved_screen()
        else:
            logger.debug('soiw.get_screen, not solved')
            return GameWindow.get_screen(self)

    def get_solved_screen(self):
        # this method must only be called when self.is_solved()
        return self.soi.solved_screen

    def get_border_color(self):
        """Return a nice yellow for the border"""
        return 248, 248, 0
