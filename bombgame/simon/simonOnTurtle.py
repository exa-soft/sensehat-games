"""
Class for playing "Simon says" using the screen (output is done with turtle graphics)
and the keyboard for input. Restarts if a wrong input is done.
"""

import logging, time
from .simonsays import SimonSays
from .simonTurtle import SimonTurtle

logger = logging.getLogger('simon.onturtle')


# noinspection PyPep8Naming
class SimonOnTurtle(SimonSays, SimonTurtle):

    def __init__(self, length):
        SimonSays.__init__(self, length)
        logger.info('simonSays length:'.format(self._size))
        SimonTurtle.__init__(self)

    def onRestart(self):
        """Reset screen on restart
        (overwritten method from SimonSays)"""
        SimonTurtle.reset(self)
        logger.info('neue Lösung bereit auf Turtle (Länge: {})'.format(self._size))

    def beforeSayingColors(self):
        """Message to user that Simon will say something
        (overwritten method from SimonSays)"""
        SimonTurtle.startSaying(self)

    def beforeHearingColors(self):
        """Start to listen to user
        (overwritten method from SimonSays)"""
        SimonTurtle.startListening(self)

    def sayColor(self, colorNum):
        """Display one color "said" by Simon
        (overwritten method from SimonSays)"""
        SimonTurtle.showColor(self, colorNum)

    def roundSolved(self):
        """Finish round
        (overwritten method from SimonSays)"""
        SimonTurtle.roundSolved(self)

    def wrongColor(self):
        """Display error to user on wrong input
        (overwritten method from SimonSays)"""
        SimonTurtle.writeResult(self, False)
        time.sleep(3)
        self.restart()

    def gameSolved(self):
        """Display message to user that game solved
        (overwritten method from SimonSays)"""
        SimonTurtle.writeResult(self, True)

    def receivedColor(self, colorNum):
        """Notify SimonSays about received color
        (overwritten method from SimonTurtle)"""
        logging.info('received color: {}'.format(colorNum))
        SimonSays.hearColor(self, colorNum)


def _test():
    tusi = SimonOnTurtle(3)
    # tusi = ss.SimonSays(4)
    # logging.info ("state: ", tusi.state)
    tusi.restart()


if __name__ == "__main__":      # to start this: see below
    # instead, use:
    # python
    # >>> from bombgame.simon import simonOnTurtle as sot
    # (you may set logging here, like:
    # >>> import logging
    # then use the lines as below (sot.logger instead of logger)
    # >>> sot._test()

    logger.setLevel(logging.INFO)
    logger.addHandler(logging.StreamHandler())
    _test()
