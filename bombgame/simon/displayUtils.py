import logging
from enum import IntEnum, unique
from typing import Tuple

from bombgame.accele_input.inclinationMeter import Direction
from bombgame.core import exceptions as coreexcept
from bombgame.core.common import SenseHat
from bombgame.core.utils import sleep_but_getaccelero
from bombgame.output.displayUtils import draw_corners, Color

ColorPair = Tuple[Color, Color]

logger = logging.getLogger('simon.display')


@unique
class Brightness(IntEnum):
    OFF = -1
    WEAK = 0
    BRIGHT = 1


# Possible colors are in the RGB-565-space (red 5 bits, green 6 bits, blue 5 bits).
# Therefore: red and blue part must be a multiple of 8, green part a multiple of 4.
_n = (0, 0, 0)       # nothing       # all are of type Color
_y0 = (128, 128, 0)  # yellow (darker)
_y1 = (248, 252, 0)  # yellow (bright)
_r0 = (128, 0, 0)    # red (darker)
_r1 = (248, 0, 0)    # red (bright)
_g0 = (0, 128, 0)    # green (darker)
_g1 = (0, 252, 0)    # green (bright)
_b0 = (0, 32, 128)   # blue (darker)
_b1 = (0, 64, 248)   # blue (bright)

# Define colors for each of the directions (first array dimension), except for Direction.NONE
# second dimension: 0 is the weak color, 1 is the strong one.
# So the weak color for direction left is _colors[3][0] or _colors[Direction.LEFT][Brightness.WEAK]
_colors = [         # with typing: _colors: List[ColorPair] = [...
    (_r0, _r1),     # red for forward
    (_b0, _b1),     # blue for right
    (_y0, _y1),     # yellow for backward
    (_g0, _g1)      # green for left
]


def blink_corners(sense: SenseHat, color: Color) -> None:
    for x in range(0, 5):
        draw_corners(sense, color)
        sleep_but_getaccelero(0.2)
        draw_corners(sense, _n)
        sleep_but_getaccelero(0.1)


def display_quadrant(sensehat: SenseHat, direction: Direction, step: int, brightness: Brightness) -> None:
    """Display a line in the given quadrant.
    Parameters:
        :param sensehat: SenseHAT to draw on
        :param direction: a direction
        :param step: 0 for along the edge, 1 for 1 away from the edge, etc. (max 3)
        :param brightness: 1 for for stronger color, 0 for less bright color, -1 to switch off
    """

    if direction == Direction.NONE:     # just to make sure that we do not try something impossible...
        return

    logger.debug('_display_quadrant: dir {0}, step {1}, bright {2}'.format(direction, step, brightness))

    if step < 0 or step > 3:
        raise coreexcept.ArgumentError(step, 'step must be between 0 and 3')

    if brightness < -1 or brightness > 1:
        raise coreexcept.ArgumentError(brightness, 'brightness must be -1, 0 or 1')

    if direction == Direction.FORWARD or direction == Direction.BACKWARD:
        _display_quad_vertical(sensehat, direction, step, brightness)
    elif direction == Direction.RIGHT or direction == Direction.LEFT:
        _display_quad_horizontal(sensehat, direction, step, brightness)
    else:
        raise coreexcept.ArgumentError(direction, 'direction must be between 0 and 3')


def display_border(sensehat: SenseHat, on: bool) -> None:
    """
    Display the four borders (along the edge), clear the corners
    :param sensehat: SenseHAT to draw on
    :param on: true to display, off to clear
    """
    for direct in Direction:
        if direct != Direction.NONE:
            if on:
                display_quadrant(sensehat, direct, 0, Brightness.WEAK)
            else:
                display_quadrant(sensehat, direct, 0, Brightness.OFF)
    sensehat.set_pixel(0, 0, _n)
    sensehat.set_pixel(0, 7, _n)
    sensehat.set_pixel(7, 0, _n)
    sensehat.set_pixel(7, 7, _n)


def _getcolor(direction: Direction, brightness: Brightness) -> Color:
    color = _n   # color for Brightness.OFF
    if brightness > Brightness.OFF:
        color = _colors[direction][brightness]
    return color


def _display_quad_vertical(sensehat: SenseHat, direction: Direction, step: int, brightness: Brightness) -> None:
    """Display a line in the given vertical quadrant (forward or backward).
    Parameters:
        :param sensehat: SenseHAT to draw on
        :param direction: only 0 (forward) and 2 (backward) are allowed
        :param step: see _display_quadrant
        :param brightness: see _display_quadrant
    """

    assert (direction == Direction.FORWARD or direction == Direction.BACKWARD)

    color = _getcolor(direction, brightness)
    row = 0 + step     # with typing: int
    if direction == 2:
        row = 7 - step

    for i in range(1 + step, 7 - step):
        sensehat.set_pixel(i, row, color)


def _display_quad_horizontal(sensehat: SenseHat, direction: Direction, step: int, brightness: Brightness) -> None:
    """Display a line in the given horizontal quadrant (left or right).
    Parameters:
        :param sensehat: SenseHAT to draw on
        :param direction: only 1 (right) and 3 (left) are allowed
        :param step: see _display_quadrant
        :param brightness: see _display_quadrant
    """

    assert (direction == Direction.RIGHT or direction == Direction.LEFT)

    color = _getcolor(direction, brightness)
    col = 0 + step      # with typing: int
    if direction == 1:
        col = 7 - step

    for i in range(1 + step, 7 - step):
        sensehat.set_pixel(col, i, color)


def display_full(sensehat: SenseHat, direction: Direction, brightness: Brightness) -> None:
    """Display the whole inner square in the line of the given "direction".
    Parameters:
        :param sensehat: SenseHAT to draw on
        :param direction: a Direction (only used for the color)
        :param brightness: a Brightness
    """

    if direction == Direction.NONE:     # do not display full if direction is NONE
        logger.debug('display_full: dir {0}, bright {1} --> skipping'.format(direction, brightness))
        return

    logger.debug('display_full: dir {0}, bright {1}'.format(direction, brightness))

    if brightness < -1 or brightness > 1:
        raise coreexcept.ArgumentError(brightness, 'brightness must be -1, 0 or 1')

    color = _getcolor(direction, brightness)
    for row in range(1, 7):
        for col in range(1, 7):
            sensehat.set_pixel(col, row, color)


# test code ###########################################

_s = SenseHat()


def _init_test():
    _s.clear()
    _s.low_light = False


def _test1() -> None:

    _init_test()
    for direction in Direction:
        print('testing direction {}'.format(direction))

        display_quadrant(_s, direction, 0, Brightness.WEAK)
        sleep_but_getaccelero(1)
        display_quadrant(_s, direction, 0, Brightness.OFF)
        sleep_but_getaccelero(0.5)
        display_quadrant(_s, direction, 0, Brightness.BRIGHT)
        sleep_but_getaccelero(1)
        display_quadrant(_s, direction, 0, Brightness.OFF)
        sleep_but_getaccelero(1.5)


def _test2() -> None:

    _init_test()
    blink_corners(_s, (124, 124, 124))
    for direct in Direction:
        display_quadrant(_s, direct, 0, Brightness.BRIGHT)

    speed = 0.2
    # direction = Direction.FORWARD
    for direction in Direction:
        print('testing direction {}'.format(direction))

        display_quadrant(_s, direction, 1, Brightness.WEAK)
        sleep_but_getaccelero(speed)
        display_quadrant(_s, direction, 1, Brightness.BRIGHT)
        sleep_but_getaccelero(speed)
        display_quadrant(_s, direction, 2, Brightness.WEAK)
        sleep_but_getaccelero(speed)
        display_quadrant(_s, direction, 2, Brightness.BRIGHT)
        display_quadrant(_s, direction, 1, Brightness.WEAK)
        sleep_but_getaccelero(speed)
        display_quadrant(_s, direction, 3, Brightness.WEAK)
        sleep_but_getaccelero(speed)
        display_quadrant(_s, direction, 3, Brightness.BRIGHT)
        display_quadrant(_s, direction, 2, Brightness.WEAK)
        sleep_but_getaccelero(speed)
        display_full(_s, direction, Brightness.BRIGHT)
        sleep_but_getaccelero(1)
        display_full(_s, direction, Brightness.WEAK)
        sleep_but_getaccelero(speed)
        display_full(_s, direction, Brightness.OFF)


def _test_orient() -> None:

    orientation = _s.get_orientation_degrees()
    print("p: {pitch}, r: {roll}, y: {yaw}".format(**orientation))


if __name__ == "__main__":      # to start this: `python3 -m bombgame.simon.displayUtils`
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    # _test1()
    _test2()
