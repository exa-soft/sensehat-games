import logging
from bombgame.core.common import SenseHat
from bombgame.accele_input.inclinationMeter import Direction

from bombgame.output.displayUtils import white_almost, display_stroke, display_correct
from bombgame.simon.simonsays import SimonSays
import bombgame.simon.displayUtils as dUt
import bombgame.output.displayUtils as out
from bombgame.simon.simonInclinationMeter import SimonInclinationMeter

sense = SenseHat()
logger = logging.getLogger('simon.inclin')


# Possible colors are in the RGB-565-space (red 5 bits, green 6 bits, blue 5 bits).
# Therefore: red and blue part must be a multiple of 8, green part a multiple of 4.


class SimonOnInclination(SimonSays):

    _meter = None           # _meter: InclinationMeter
    _round_solved = False   # _round_solved: bool
    _run_in_gamewindow = True
    # if this is True, listen will not call a loop, but only check once
    # (looping must then be done externally, like by the GameWindow)
    solved_screen = [out.Color] * 64   # cache screen of solved game, only valid when all rounds solved

    def __init__(self, length: int) -> None:
        SimonSays.__init__(self, length)
        # self._sense = sense
        self._meter = SimonInclinationMeter(sense, self.receivedColor)
        logger.info('SimonOnInclination, length:'.format(length))

    def onRestart(self) -> None:
        """Reset screen on restart
        (overwritten method from SimonSays)"""
        self._meter.clear()
        self._round_solved = False
        logger.info("neue Lösung bereit (Länge: {})".format(self._size))

    def beforeSayingColors(self) -> None:
        """Message to user that Simon will say something
        (overwritten method from SimonSays)"""
        sense.clear()
        dUt.sleep_but_getaccelero(0.3)
        dUt.blink_corners(sense, white_almost)
        dUt.sleep_but_getaccelero(0.3)

    def beforeHearingColors(self) -> None:
        """Start to listen to user: Make ready to receive input from the user and display it.
        (overwritten method from SimonSays)"""
        self._meter.clear()
        self._meter.ignore_values(40)
        sense.clear()
        dUt.display_border(sense, True)
        logger.debug('start measuring inclination (size {0}, curLen {1})'.format(self._size, self._curLen))
        self.listen()

    def listen(self) -> None:
        """Listen to user input.
        This checks the inclination values in a loop, running while
        isListening() returns true and the game is not yet solved.
        """
        if self.isListening() and self._curLen <= self._size:
            self._meter.update()    # measure direction and steepness and display it
            # dUt.sleep_but_getaccelero(0.2)   # do not sleep - constantly measure and display!
            if not self._run_in_gamewindow:     # if not in a GameWindow, we have to do the looping
                self.listen()

    def sayColor(self, colorNum: int) -> None:
        """Display one color "said" by Simon
        (overwritten method from SimonSays)"""
        direction = Direction(colorNum)
        dUt.display_full(sense, direction, dUt.Brightness.BRIGHT)
        dUt.sleep_but_getaccelero(0.7)
        dUt.display_full(sense, direction, dUt.Brightness.OFF)
        dUt.sleep_but_getaccelero(0.3)

    def roundSolved(self) -> None:
        """Finish round (overwritten method from SimonSays)"""
        logger.debug('round solved, stop measuring')
        # only play "round solved" animation if not whole game has been solved
        if self._curLen < self._size:
            display_correct(sense)

    def wrongColor(self) -> None:
        """Display error to user on wrong input
        (overwritten method from SimonSays)"""
        logger.debug('wrong color, stop measuring')
        display_stroke(sense, True)      # display an error (True = clear the screen first)
        dUt.sleep_but_getaccelero(1)
        self.restart()

    def gameSolved(self) -> None:
        """Display message to user that game solved
        (overwritten method from SimonSays)"""
        logger.info('game solved, stop measuring')
        out.display_gamesolved(sense)
        dUt.display_border(sense, True)
        out.draw_circle(sense, 3, out.green1)
        # if the final screen has not yet saved, save it
        self.solved_screen = sense.get_pixels()
        logger.debug('cached solved screen: {}'.format(self.solved_screen))


    def is_solved(self) -> bool:
        """Return if the game is solved (is needed for display in GameWindow)
        :return:    True if the game has been solved (all rounds)
        """
        return self._curLen > self._size

    # noinspection PyPep8Naming
    def receivedColor(self, _sense: SenseHat, colorNum: int) -> None:
        """Notify SimonSays about received color (callback method from inclination meter"""
        # no need to care about whether we are listening or not - this is done by simonsays
        logger.info('received color: {}'.format(colorNum))
        SimonSays.hearColor(self, colorNum)


def _test():
    _s = SenseHat()
    sis = SimonOnInclination(3)
    sis._run_in_gamewindow = False      # for testing outside of a GameWindow
    sis.restart()


def exit_handler(sig, frame):
    """
    Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    # _s.clear()
    raise SystemExit


if __name__ == "__main__":      # to start this: `python3 -m bombgame.simon.simonOnInclination`
    # Set up signal handler to catch Ctrl-c
    from signal import signal, SIGINT
    signal(SIGINT, exit_handler)

    f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.INFO, format=f)
    logger.setLevel(logging.DEBUG)
    # logger.addHandler(logging.StreamHandler())

    _test()
