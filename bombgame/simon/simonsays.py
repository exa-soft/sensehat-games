# state machine module

import logging
from typing import List, ClassVar, Callable
from transitions import Machine
from random import Random

logger = logging.getLogger('simon.says')


# noinspection PyPep8Naming
class Round(object):
    """Class to wrap a round inside a SimonSays game
        Has a list of states and a machine for them (finite states machine)
        """
    solution = []   # solution: List[int]
    size = 0        # size: int
    states = []     # states: List[str]
    machine = None  # machine: Machine

    def __init__(self, solution: List[int], solvedCallback: Callable[[], None]) -> None:
        """init state-machine for this round: states correct0, correct1, etc. up to length of solution"""
        self.solution = solution
        self.size = len(solution)
        logger.debug('initMachine, size: {}'.format(self.size))
        self.states = ["correct" + str(i) for i in range(self.size + 1)]
        logger.debug('states are: {}'.format(self.states))
        self.machine = Machine(model=self, states=self.states, initial='correct0', ignore_invalid_triggers=True)
        # ignoring invalid triggers will just return False if state transition not possible
        # -> then we can report error in game (no need for transitions back to the inital state, because if the user
        # gives a wrong answer, he/she has to start over (new game!)
        
        for i in range(self.size):
            triggerName = 'gotColor' + str(self.solution[i])
            sourceName = 'correct' + str(i)
            destName = 'correct' + str(i + 1)
            if i == self.size - 1:
                self.machine.add_transition(triggerName, source=sourceName, dest=destName, after=solvedCallback)
            else:
                self.machine.add_transition(triggerName, source=sourceName, dest=destName)
            logger.debug('added transition {0} from state "{1}" to state "{2}"'
                          .format(triggerName, sourceName, destName))

        logger.debug('added callback on entering {0} '.format('correct' + str(self.size)))
        logger.debug('state-machine initialized')

    def __str__(self) -> str:
        return 'Round in SimonSays (length {sf.size}, solution {sf.solution})'.format(sf=self)


# noinspection PyPep8Naming
class SimonSays(object):
    """Class for a game of SimonSays on a vocabulary of four possible values (0, 1, 2, 3)
    To play a game:
    - init the object with the length of the final solution, e.g. `simon = SimonSays(5)`
    - call `simon.restart()` to start the game (this will start 'saying')
    - listen to user input and give it (as a number from 0..3) to simon using `simon.hearColor(the number)`
    Important:
    - The object will not contain a solution before `restart()` is called for the first time.
    - Simon will not listen (method `hearColor()` will not do anything) while he is saying something.
      In detail: simon will stop listening just before `beforeSayingColor()` and start listening just before
      `beforeHearingColor()`. Simon will also stop listening if a wrong color has been heard or when a round
      or the whole game has been finished (thus, before calling `wrongColor()` and before `roundSolved()`
      or `displayGameSolved()`, respectively). The state of listening can be asked with `isListening()`.
    See also example in  `test()` or the example implementation `simonOnTurtle`.
    Subclasses should overwrite:
    - `onRestart()`
    - `beforeSayingColors()`
    - `beforeHearingColors()`
    - `sayColor(color)`
    - `roundSolved()`
    - `wrongColor()`
    - `displayGameSolved()`
    """

    _size = -1          # _size
    _curLen = -1        # _curLen: int
    _solution = []      # _solution: List[int]
    _listening = False  # _listening: bool

    def __init__(self, length: int):
        """init a SimonSays game with the given maximal length"""
        self._size = length
        # self._reset()

    def _reset(self):
        """reset internal variables (new solution)"""
        logger.debug('_reset')
        self._listening = False
        self._solution = _random_solution(self._size)
        logger.info("solution is: {}".format(self._solution))
        self._curLen = 0

    def restart(self):
        """start or restart the game (with a new solution)"""
        self._reset()
        self.onRestart()
        self._nextRound()

    def onRestart(self):
        """ maybe do something when a new game is started"""
        logger.info("restart, new solution is: {}".format(self._solution))
        # TODO we want to plug-in our own method for displaying the "colors"

    def _nextRound(self):
        """start the next round ('saying' something which is 1 longer than before)"""
        logger.debug('nextRound')
        self._curLen += 1
        self._initMachine(self._curLen)
        self._say()

    def repeatRound(self):
        """Start the dame round again ('saying' the same as before). This can be used
        if a game can be paused, to say the same again when the game is resumed.
        """
        logger.debug('repeatRound (len {})'.format(self._curLen))
        self._initMachine(self._curLen)
        self._say()

    def beforeSayingColors(self):
        """ maybe do something before saying the 'colors'"""
        print("Pass auf! Hier kommt's:")
        # TODO we want to plug-in our own method for displaying the "colors"

    def beforeHearingColors(self):
        """ maybe do something to start 'listening' to the 'colors'"""
        print("Jetzt bist du dran")
        # TODO we want to plug-in our own method for starting to listen

    def sayColor(self, color):
        """display one color - this method should be given / plugged in by the class using SimonSays"""
        print('- ', color)
        # TODO we want to plug-in our own method for displaying the "colors"

    def _say(self):
        """simon says the 'sentence' of the current length"""
        logger.debug('say')
        self._listening = False
        self.beforeSayingColors()
        for i in range(self._curLen):
            self.sayColor(self._solution[i])
        self._listening = True
        self.beforeHearingColors()

    def isListening(self):
        return self._listening

    def hearColor(self, color):
        """simon hears the other player say a color (0..3)"""
        if not self._listening:
            logger.debug('hear color: {} - ignored (not listening now)'.format(color))
        else:
            logger.debug('hear color: {}'.format(color))
            # We have set the state machine to just return false in cause of unknown triggers,
            # but sometimes it raises an AttributeError nevertheless. So we have to catch it here:
            try:
                res = self.round.trigger('gotColor' + str(int(color)))
                if not res:
                    self._listening = False
                    self.wrongColor()
            except AttributeError:
                self._listening = False
                self.wrongColor()

    def roundSolved(self):
        print("------ richtig --------")
        # TODO we want to plug-in our own method for displaying "roundSolved"

    def wrongColor(self):
        print("-----!!! falsch !!!-----------")
        # TODO we want to plug-in our own method for displaying "wrong"

    def _solved(self):
        logger.debug('solved')
        self._listening = False
        self.roundSolved()
        if self._curLen < self._size:
            self._nextRound()
        else:
            # make sure that we do not play the last round again (when played in a window)
            self._curLen += 1
            self.gameSolved()

    def gameSolved(self):
        """the game has been solved up to the maximal length"""
        print("------- >>> du bist super! <<<-------")
        # TODO we want to plug-in our own method for what to do when game is solved

    def _initMachine(self, size: int):
        """init state-machine for this round: states correct0, correct1, etc. up to size"""
        roundSolution = self._solution[0:size]              # roundSolution: List[int]
        logger.debug('solution for round is {}'.format(roundSolution))
        self.round = Round(roundSolution, self._solved)     # self.round: ClassVar[Round]

    def __str__(self):
        return 'SimonSays (final length {s._size}, current length {s._curLen}, solution {s._solution})'\
            .format(s=self)


def _random_solution(size: int) -> List[int]:
    r = Random()
    r.seed()
    sol = []
    for i in range(size):
        sol.append(r.randint(0, 3))
    return sol


# noinspection PyPep8Naming
def _testAuto(testlen):
    simon = SimonSays(testlen)      # simon: SimonSays
    print('[testout]', simon)
    simon.restart()

    for curTestLen in range(testlen):
        # we know the solution... - say numbers up to current length
        for i in range(simon._curLen):
            simon.hearColor(simon._solution[i])
            print('[testout] state: ', simon.round.state)


def _test():
    print('demo for simonsays with 3 colors:')
    _testAuto(3)


if __name__ == "__main__":      # to start this: `python3 -m bombgame.simon.simonsays`
    f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.INFO, format=f)
    logger.setLevel(logging.WARNING)
    _test()
