from ..core.common import SenseHat
from enum import IntEnum, unique
from math import degrees
from typing import Tuple, Callable
import logging

# LATER we could save and display both the left/right and backward/forward direction, but only care about the bigger one

logger = logging.getLogger('accele.meter')


@unique
class Steepness(IntEnum):
    NEUTRAL = 0
    LOW = 1
    MIDDLE = 2
    HIGH = 3


@unique
class Direction(IntEnum):
    NONE = -1       # neutral
    FORWARD = 0     # forward (away from me)
    RIGHT = 1
    BACKWARD = 2    # backward (towards me)
    LEFT = 3


Inclination = Tuple[Direction, Steepness]


INCLI_STEP_SIZE = 7         # how many degrees make a change in the display
INCLI_IGNORE_ABOVE = 60
READINGS_PER_VALUE = 3      # how many readings of the sensors are combined into one value


def _get_steepness(degrees: float) -> Steepness:
    absdegrees = abs(degrees)        # absdegrees: float
    if absdegrees > 3 * INCLI_STEP_SIZE:
        return Steepness.HIGH
    elif absdegrees > 2 * INCLI_STEP_SIZE:
        return Steepness.MIDDLE
    elif absdegrees > INCLI_STEP_SIZE:
        return Steepness.LOW
    else:
        return Steepness.NEUTRAL


def get_inclination(sensehat: SenseHat) -> Inclination:
    """
    Reads the orientation of the Pi and evaluates if it is pitched or rolled.
    This is returned as a Inclination (a Tuple[Direction, Steepness]).
    Values over 45° are ignored. Less than 10° counts as neutral
    :param sensehat: the SenseHAT to analyze
    :return: Tuple[Direction, Steepness]
    """

    pitch_list = []
    roll_list = []
    for i in range(READINGS_PER_VALUE):
        # get_orientation_degrees normalizes the values from 0 to 360, but we
        # want values from -180 to 180, so get radians and normalize ourselves.
        o = sensehat.get_orientation_radians()
        pitch_list.append(o["pitch"])
        roll_list.append(o["roll"])

    pitch = degrees(sum(pitch_list) / len(pitch_list))   # pitch: float
    roll = degrees(sum(roll_list) / len(roll_list))      # roll: float

    # do not care about values > INCLI_IGNORE_ABOVE° (in each direction)
    if abs(pitch) > INCLI_IGNORE_ABOVE or abs(roll > INCLI_IGNORE_ABOVE):
        logger.debug('get_inclination: values too high')
        return Direction.NONE, Steepness.NEUTRAL

    # look at the biggest absolute value (from pitch and roll)
    if abs(pitch) > abs(roll):
        # look if left or right
        direct = Direction.LEFT      # direct: Direction
        if pitch > 0:
            direct = Direction.RIGHT
        steep = _get_steepness(pitch)    # steep: Steepness
        logger.debug('get_inclination: pitch {0} evalutated to {1} / {2}'.format(pitch, direct.name, steep.name))

    else:
        # look if forward or backward (away from me or towards me)
        direct = Direction.BACKWARD      # direct: Direction
        if roll > 0:
            direct = Direction.FORWARD
        steep = _get_steepness(roll)     # steep: Steepness
        logger.debug('get_inclination: roll {0} evalutated to {1} / {2}'.format(roll, direct.name, steep.name))

    logger.debug('found inclination {0} / {1}'.format(direct.name, steep.name))
    return direct, steep


class InclinationMeter(object):
    """Class to wrap the analysis and display of the inclination of a SenseHAT.
    Values over 30° can raise a callback (a method that takes a SenseHAT and a direction as parameter).
    Subclasses must  overwrite:
    - update(self):  uodates the display according to the measured

    """

    # TODO maybe move _cur and _prev to SimonInclinationMeter
                        # with typing:
    _sense = None       # _sense: ClassVar[SenseHat]
    _cur = None         # _cur: ClassVar[Inclination]  # current inclination (direction & steepness)
    _prev = None        # _prev: ClassVar[Inclination]  # previous inclination

    def __init__(self, sensehat: SenseHat) -> None:
        """
        Init the inclination meter for the given SenseHAT.
        :param sensehat: SenseHAT
        :param call_for_dir: function to be called when the full inclination for a direction has been reached.
            (function must take a SenseHAT and a Direction)
        """
        self._sense = sensehat      # ClassVar[SenseHat]
        logger.debug('init')
        self.clear()
        logger.info('InclinationMeter initialized')

    def __str__(self) -> str:
        return 'InclinationMeter'

    def clear(self) -> None:
        """
        Clear the screen.
        Subclasses should overwrite this method to change the behaviour
        and/or clear additional variables.
        """
        self._sense.clear()

    def ignore_values(self, num_values) -> None:
        """
        Ignores the given number of accelerometer reading. Can be used to "empty the queue"
        of the meter (if not reading the values all the time, it seems that we get old values
        when we start to read again).
        :param num_values:  number of readings to ignore
        """
        for i in range(num_values):
            self._sense.get_orientation_radians()

    def update(self) -> None:
        """
        Subclasses must overwrite this method.
        Measures the inclination of the SenseHAT and adjust the display accordingly.
        The display can also take into account the previous situation, but is responsible to set that.
        This method can be called in a loop in quick succession.
        """
        logger.info('update (should be overwritten by subclass!)')


def _test_normalize():
    import math
    _s = SenseHat()
    for i in range(200):
        deg = _s.get_orientation_degrees()      # this is converted to 0..360
        rad = _s.get_orientation_radians()      # this is -1..1
        for key, val in rad.items():
            rad[key] = math.degrees(val)        # Result is -180 to +180
        logger.debug('deg: {}, converted rad: {}'.format(deg, rad))


if __name__ == "__main__":      # to start this: `python3 -m bombgame.accele_input.inclinationMeter`
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    _test_normalize()
