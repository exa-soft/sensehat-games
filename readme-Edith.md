Infos zur Benutzung & Entwicklung
===

Entwicklung
---

1. In shell ins Projekt-Verzeichnis wechseln

1. Python-Version kontrollieren: `pyenv versions`

1. virtual-environment benutzen: `source venv353/bin/activate`
   Dann sollte  `python -V`  dasselbe sein wie  `python3 -V` . 

1. Python-Scripts benutzen mit 

   ```shellscript  
   python  
   >>> (from abc) import xyz as x  
   >>> x._test()  
   ```

   oder  `python -m abc.xyz` (**nicht `python abc/xyz.py`**, gibt Import-Probleme!)  
   Beispiele:
   
   * `python output/pixelUtils.py`  (hier keine kritischen relativen Imports, also geht es ohne -m) 
   * `python -m output.displayUtils`
   Beachten: auf RasPi immer `python3` benutzen (dort gibt es kein venv, python zeigt auf Python2).
   
1. Zum Beenden der Python-Shell Ctrl-d (Ctrl-c, um Programm zu stoppen)

1. Ende der Benutzung von virtual-environment: `deactivate`

Git
---
 
_(Auf Rasbian geht SmartGit nicht...)_  
Gute Cheatsheets:
 
 - https://www.git-tower.com/blog/git-cheat-sheet/
 - https://about.gitlab.com/images/press/git-cheat-sheet.pdf
 
 Allerlei Kommandi auch abgespeichert in Textfile auf RasPi.
 
  


Installation von Sense HAT & Sense HAT Emulator in Ubuntu
---

Gemäss https://sense-emu.readthedocs.io/en/v1.1/install.html

Achtung auf Teil virtual environments! Also venv muss mit `--system-site-packages` benutzt worden sein:  
`python -m venv --system-site-packages <DIR>`

Dann: Activate your virtual environment and install the Sense HAT Emulator:

```shellscript   
source <DIR>/bin/activate
pip install sense-emu
pip install sense-hat  
(oder: 
pip install sense-emu sense-hat)
```

Emulator starten mit Shortcut unter "Anwendungen - Entwicklung - Sense HAT Emulator".  
Darin "File - Open Example - ..." schreibt die Beispiele ins home-Verzeichnis 
(aus https://magpi.raspberrypi.org/articles/sense-hat-emulator).

Weitere verwendete Tools installieren: 

```shellscript
pip install transitions
```

Programm automatisch starten beim Start des RasPi
---

_(aus https://pisense.readthedocs.io/en/latest/maze.html)_

Speichern als `bombgame.service` in `/etc/systemd/system/` (Achtung, Pfad bei ExecStart ggf. anpassen):

```shellscript
[Unit]
Description=The SenseHAT game Keep talking and nothing explodes
After=local-fs.target

[Service]
ExecStart=/usr/bin/python3 /home/pi/bin/bombgame/gameStarter.py
User=pi

[Install]
WantedBy=multi-user.target
```

Mit Shell-Kommandos: 

- Service aktivieren: `sudo systemctl enable bombgame`  
- Service deaktivieren: `sudo systemctl disable bombgame`


Logging hints
---

from https://docs.python.org/3.6/howto/logging-cookbook.html:

```python
# create file handler which logs even debug messages
fh = logging.FileHandler('spam.log')
fh.setLevel(logging.DEBUG)

# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)

# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
```
  
------

Allgemeine Infos zu den verwendeten Tools
---

**Mehrere Python-Versionen** installieren und benutzen (nur auf Ubuntu, nicht auf Rasbian):

* pyenv installieren gemäss https://github.com/pyenv/pyenv-installer
* Python 3.5.3 installieren (siehe Kommandi unten)
* Python-Version lokal auf 3.5.3 setzen: im Projekt-Directory `pyenv local 3.5.3`
* nützliche pyenv-Kommandi: https://github.com/pyenv/pyenv/blob/master/COMMANDS.md

**virtual environment**:
 
* erstellen: `python -m venv --system-site-packages <DIR>`  (python muss auf richtige Python-Version zeigen)  
* aktivieren: `source <DIR>/bin/activate`
* aus:   
https://packaging.python.org/tutorials/installing-packages/#creating-virtual-environments

### Installierte Packages

`python -m pip install SomePackage`

* nicht verfügbar auf Python 3.5: unittest.  
  Die Tests laufen in Python 3.6  

* unittest: `python -m pip install unittest`

-------
unbenutzt:

Installation eines GRID Editors
---

Klonen von https://github.com/jrobinson-uk/RPi_8x8GridDraw, ins Verzeichnis wechseln.

pip systemweit installieren, dann pypng und pygame herunterladen/installieren:
```
sudo apt-install python-pip
sudo pip install pypng
sudo pip install pygame
```

Änderung um mit Emulator zu laufen:
in sense_grid.py ändern: 
```
# from sense_hat import AstroPi
from sense_emu import AstroPi
```

Starten mit `python sense_grid.py`
Allerdings wird so das Bild nicht auf den Emulator übertragen?!

