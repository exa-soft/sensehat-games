# from sense_hat import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED, DIRECTION_DOWN, DIRECTION_UP
# from sense_emu import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED, DIRECTION_DOWN, DIRECTION_UP
from bombgame.core.common import SenseHat, ACTION_PRESSED, ACTION_HELD, ACTION_RELEASED, DIRECTION_DOWN, DIRECTION_UP
from signal import signal, SIGINT
import time
import logging

logger = logging.getLogger('joysticktest')


def scroll_up():
    logger.info('scrolling up: start...')
    time.sleep(1)
    logger.info('scrolling up: finished.')


def scroll_down():
    logger.info('scrolling down: start...')
    time.sleep(2)
    logger.info('scrolling down: finished.')


def handle_event(_sense, _event):
    logger.debug("joystick was {} {}".format(_event.action, _event.direction))
    if _event.action != ACTION_RELEASED:
        if _event.direction == DIRECTION_DOWN:
            scroll_down()
        elif _event.direction == DIRECTION_UP:
            scroll_up()
        else:
            logger.debug('nothing implemented for this direction')


def exit_handler(sig, frame):
    """
    Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    sense.clear()
    raise SystemExit


# Set up signal handler to catch Ctrl-c
signal(SIGINT, exit_handler)


if __name__ == "__main__":      # to start this: `python3 -m tests.manualtests.joystickTest`
    logging.getLogger().setLevel(logging.INFO)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())

    sense = SenseHat()
    i = 0
    while True:
        event = sense.stick.wait_for_event(emptybuffer=True)
        handle_event(sense, event)
        i += 1
        time.sleep(0.1)
        logger.debug('handled move {}'.format(i))