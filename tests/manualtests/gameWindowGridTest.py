"""
This module contains the "window collection", which is an imaginary
rectangle of several "GameWindows", each with its game, so the user can
move from one to another by using the joystick. The whole can be used
to implement games like "Keep talking and nobody explodes".

Usage:

1. Change width and height of the grid, if necessary.
2. Init each place of the grid with a game (with set_game()).
3. Call start().

"""

__author__ = 'Edith Birrer'
__version__ = '0.3'


import logging
import time
from signal import pause, signal, SIGINT
from bombgame.core.common import SenseHat, ACTION_RELEASED
from bombgame.core.game import GameWindow
from bombgame.core.gameWindowGrid import GameWindowGrid
from bombgame.dummy.dummyGame import DummyGame
from bombgame.dummy.countdownGame import CountdownGame
from bombgame.simon.simonOIWindow import SimonOIWindow
from bombgame.countdown.countdownWindow import CountdownWindow


def exit_handler(sig, frame):
    """Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    logger.info('Ctrl-C pressed')
    sense.clear()
    raise SystemExit


# Set up signal handler to catch Ctrl-c
signal(SIGINT, exit_handler)


# test code ###########################################


logger = logging.getLogger('core.gameWinGridTest')
sense = SenseHat()


def handle_event_test(_event):
    logger.debug("test: joystick was {} {}".format(_event.action, _event.direction))
    # Check if the joystick was pressed or held (but not released)
    if _event.action != ACTION_RELEASED:
        # Check which direction
        # if _event.direction == DIRECTION_UP:
        #     sense.show_letter("U")  # Up arrow
        # elif _event.direction == DIRECTION_DOWN:
        #     sense.show_letter("D")  # Down arrow
        # elif _event.direction == DIRECTION_LEFT:
        #     sense.show_letter("L")  # Left arrow
        # elif _event.direction == DIRECTION_RIGHT:
        #     sense.show_letter("R")  # Right arrow
        # elif _event.direction == DIRECTION_MIDDLE:
        #     sense.show_letter("M")  # Enter key

        # Wait a while and then clear the screen
        # time.sleep(0.5)
        # sense.clear()
        pass
    else:
        # print("other event")
        pass


def test_move_and_wait(grid, horizontal, up_or_left, sleep_time):
    """Function to shorten the test methods
    :param grid:        The gameWindowGrid to use for the moves
    :param horizontal:  True to call go_horizontal, False for go_vertical
    :param up_or_left:  True to go left or up, False to go right or down
    :param wait_time:   seconds to sleep after moving
    """
    if horizontal:
        print('go left...') if up_or_left else print('go right...')
        grid.go_horizontal(up_or_left)
    else:
        print('go up...') if up_or_left else print('go down...')
        grid.go_vertical(up_or_left)
    time.sleep(sleep_time)


def test4games():

    grid = GameWindowGrid(2, 2)
    game1 = GameWindow("A")
    game2 = GameWindow("B")
    game3 = GameWindow("C")
    game4 = GameWindow("D")
    print(grid)
    grid.set_game(0, 0, game1)
    grid.set_game(1, 0, game2)
    grid.set_game(0, 1, game3)
    grid.set_game(1, 1, game4)
    for y in range(2):
        for x in range(2):
            g = grid.get_game(x, y)
            print('game at {}/{}: {}'.format(x, y, g))

    # sense.clear()
    grid.start(sense)


def test2dummygames_horiz():

    grid = GameWindowGrid(2, 1)
    game1 = DummyGame("Apfelmus")
    game2 = DummyGame("Birewegge")
    grid.set_game(0, 0, game1)
    grid.set_game(1, 0, game2)
    y = 0
    for x in range(2):
        g = grid.get_game(x, y)
        print('game at {}/{}: {}'.format(x, y, g))
    sense.clear()
    grid.start(sense)
    pause()     # test with the joystick!


def test2countdown_vertical():

    grid = GameWindowGrid(1, 2)
    game1 = CountdownGame("Fünfer", seconds=5)
    game2 = CountdownGame("Seven", seconds=7)
    grid.set_game(0, 0, game1)
    grid.set_game(0, 1, game2)
    sense.clear()
    grid.start(sense)
    pause()     # test with the joystick!


def test2withsoi_vertical():
    """only 2 games: top and bottom, but this time, we use the SimonOnInclinatiion class."""

    grid = GameWindowGrid(1, 2)
    grid.width = 1
    grid.height = 2

    game1 = DummyGame('Top-Above')
    game2 = SimonOIWindow('Simon', len=2)
    logging.debug(grid._games)

    grid.set_game(0, 0, game1)
    grid.set_game(0, 1, game2)
    grid.start(sense)


def test2_withtimer_vertical():
    """only 2 games: top is simon, bottom is bomb"""

    grid = GameWindowGrid(1, 2)
    grid.width = 1
    grid.height = 2

    game1 = SimonOIWindow('Simon', len=2)
    game2 = CountdownWindow('countdown', duration=30)
    logging.debug(grid._games)

    grid.set_game(0, 0, game2)
    grid.set_game(0, 1, game1)
    grid.start(sense)

    allsolved = grid.all_games_solved()
    logger.info('all_games_solved was {}'.format(allsolved))


f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
logging.basicConfig(level=logging.INFO, format=f)
logging.getLogger('core.game').setLevel(logging.DEBUG)
logging.getLogger('core.gameWindowGrid').setLevel(logging.DEBUG)
logger.setLevel(logging.DEBUG)

# enable one of the tests, then test with the joystick:
# test4games()
# test2dummygames_horiz()
# test2countdown_vertical()
# test2withsoi_vertical()
test2_withtimer_vertical()

# pause()

# to start this: `python3 -m tests.manualtests.gameWindowGridTest`
