"""
This module contains the "window collection", which is an imaginary
rectangle of several "GameWindows", each with its game, so the user can
move from one to another by using the joystick. The whole can be used
to implement games like "Keep talking and nobody explodes".

Usage:

1. Change width and height of the grid, if necessary.
2. Init each place of the grid with a game (with set_game()).
3. Call start().

"""

__author__ = 'Edith Birrer'
__version__ = '0.3'


import logging
import time
from signal import pause, signal, SIGINT
from bombgame.core.common import SenseHat, ACTION_RELEASED
from bombgame.core.gameWindowGrid import GameWindowGrid
from bombgame.output.serialNumberWindow import SerialNumberWindow
from bombgame.simon.simonOIWindow import SimonOIWindow
from bombgame.countdown.countdownWindow import CountdownWindow
from bombgame.simplewires.wiresOIWindow import WiresOIWindow
from bombgame.simplewires.wiresSolution import random_serial


def exit_handler(sig, frame):
    """Signal handler to clear the screen and close the program nicely on Ctrl-c.
    Must be registered with signal(SIGINT, exit_handler).
    """
    logger.info('Ctrl-C pressed')
    sense.clear()
    raise SystemExit


# Set up signal handler to catch Ctrl-c
signal(SIGINT, exit_handler)


# test code ###########################################


logger = logging.getLogger('core.gameWinGridTest2')
sense = SenseHat()


def test2_withtimer_vertical():
    """only 2 games: top is simon, bottom is bomb"""

    grid = GameWindowGrid(1, 2)
    grid.width = 1
    grid.height = 2
    serial_no = random_serial(8)

#    game1 = SimonOIWindow('Simon', len=2)
    game2 = CountdownWindow('countdown', duration=30)
    game3 = WiresOIWindow('Wires', serial = serial_no)
    logging.debug(grid._games)

    grid.set_game(0, 0, game2)
    grid.set_game(0, 1, game3)
    grid.start(sense, 0, 0)

    allsolved = grid.all_games_solved()
    logger.info('all_games_solved was {}'.format(allsolved))


def test3_withtimer_square():
    """with 4 games (2 simple wires, 1 simon, bottom left is bomb"""

    grid = GameWindowGrid(2, 2)
    serial_no = random_serial(8)

    game0 = WiresOIWindow('Wires1', serial=serial_no)
    game1 = SimonOIWindow('Simon', len=2)
    game2 = CountdownWindow('countdown', duration=120)
    game3 = WiresOIWindow('Wires2', serial=serial_no)
    logging.debug(grid._games)

    grid.set_game(0, 0, game0)
    grid.set_game(1, 0, game1)
    grid.set_game(0, 1, game2)
    grid.set_game(1, 1, game3)
    grid.start(sense, 0, 1) # start at countdown window

    allsolved = grid.all_games_solved()
    logger.info('all_games_solved was {}'.format(allsolved))


def test4_withserial_vertical():
    """3 windows: top is wires, middle is bomb, bottom is serial"""

    grid = GameWindowGrid(1, 3)
    serial_no = random_serial(6)

    wires = WiresOIWindow('Wires', serial=serial_no)
    count = CountdownWindow('countdown', duration=60)
    serial = SerialNumberWindow('Serial', serial=serial_no)
    logging.debug(grid._games)

    grid.set_game(0, 0, wires)
    grid.set_game(0, 1, count)
    grid.set_game(0, 2, serial)
    grid.start(sense, 0, 1)

    allsolved = grid.all_games_solved()
    logger.info('all_games_solved was {}'.format(allsolved))


def test5_withserial_square():
    """
    With 4 games (simple wires, simon, bottom left is bomb, to right is serial number.
    """

    grid = GameWindowGrid(2, 2)
    serial_no = random_serial(8)

    wires = WiresOIWindow('Wires1', serial = serial_no)
    simon = SimonOIWindow('Simon', len=2)
    count = CountdownWindow('countdown', duration=120)
    serial = SerialNumberWindow('Serial', serial = serial_no)
    logging.debug(grid._games)

    grid.set_game(0, 0, serial)
    grid.set_game(1, 0, simon)
    grid.set_game(0, 1, count)
    grid.set_game(1, 1, wires)
    grid.start(sense, 0, 1) # start at countdown window

    allsolved = grid.all_games_solved()
    logger.info('all_games_solved was {}'.format(allsolved))


def test_end():
    from bombgame.core.gameWindowGrid import _show_end
    _show_end()


f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
logging.basicConfig(level=logging.INFO, format=f)
logging.getLogger('core.game').setLevel(logging.DEBUG)
logging.getLogger('core.gameWindowGrid').setLevel(logging.DEBUG)
logger.setLevel(logging.DEBUG)

# enable one of the tests, then test with the joystick:
# test_end() # no joystick
# test2_withtimer_vertical()
# test3_withtimer_square()
test4_withserial_vertical()
# test4_withserial_square()


# pause()

# to start this: `python3 -m tests.manualtests.gameWindowGridTest2`
