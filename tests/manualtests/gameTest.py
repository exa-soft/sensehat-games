"""Test for basic game "window" and"window collection" module."""

__author__ = 'Edith Birrer'
__version__ = '1.0'


import logging
from bombgame.core.common import SenseHat
from bombgame.core.game import GameWindow
from bombgame.dummy import dummyGame as dG
from bombgame.core.gameWindowGrid import GameWindowGrid

logger = logging.getLogger('core.gameTest')
sense = SenseHat()


def simple_test():
    """only 2 games: top and bottom."""
    
    grid = GameWindowGrid(1, 2)
    grid.width = 1
    grid.height = 2

    game1 = GameWindow('Top-Above')
    game2 = GameWindow('Bottom-Below')
    logging.debug(grid._games)

    grid.set_game(0, 0, game1)
    grid.set_game(0, 1, game2)
    grid.start(sense)


def dummygame_test():
    """only 2 games: top and bottom, but this time, we use the DummyGame class ."""

    grid = GameWindowGrid(1, 2)
    grid.width = 1
    grid.height = 2

    game1 = dG.DummyGame('Top-Above')
    game2 = dG.DummyGame('Bottom-Below')
    logging.debug(grid._games)

    grid.set_game(0, 0, game1)
    grid.set_game(0, 1, game2)
    grid.start(sense)


if __name__ == "__main__":      # to start this: `python3 -m tests.manualtests.gameTest`

    f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=f)
    logger.setLevel(logging.DEBUG)

    logging.getLogger().setLevel(logging.DEBUG)
    simple_test()
    # dummygame_test()

# to start the longer test:
# python
# >>> from core import gameTest as gt
# >>> import logging
# >>> logging.getLogger().setLevel(logging.INFO)
# >>> gt.dummygame_test()
