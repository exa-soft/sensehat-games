from unittest import TestCase
from bombgame.simon.simonsays import SimonSays

class TestSimonSays(TestCase):

    # noinspection PyPep8Naming
    def _testAuto(self, testlen):
        simon = SimonSays(testlen)  # simon: SimonSays
        print('[testout]', simon)
        simon.restart()

        for curTestLen in range(testlen):
            # we know the solution... - say numbers up to current length
            for i in range(simon._curLen):
                simon.hearColor(simon._solution[i])
                print('[testout] state: ', simon.round.state)

    def test_game_solved(self):
        self._testAuto(3)
        self._testAuto(4)
        self._testAuto(5)
