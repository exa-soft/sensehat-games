import logging
import string
from unittest import main, TestCase

from bombgame.simplewires.colors import WireColor
from bombgame.simplewires.wiresSolution import getSolution, _getLastDigit, random_serial

f = '%(relativeCreated)6d %(threadName)-12s %(name)-20s %(levelname)-6s %(message)s'
logging.basicConfig(level=logging.INFO, format=f)
logger = logging.getLogger('wires.solution.test')
logger.setLevel(logging.DEBUG)
# to switch on logging for tested class:
# logging.getLogger('wires.solution').setLevel(logging.DEBUG)


class Test(TestCase):

    def test_getLastDigit(self):
        self.assertEqual('0', _getLastDigit('7890'))
        self.assertEqual('4', _getLastDigit('1234asdf'))
        self.assertEqual('4', _getLastDigit('a1s2d3f4g'))
        self.assertEqual('7', _getLastDigit('9L8K7JHGF'))
        self.assertEqual('', _getLastDigit('EMPBERFQ'))
        self.assertEqual('', _getLastDigit(''))

    def test_random_serial(self):
        for i in range(20):
            r = random_serial(4)
            self.assertIsNotNone(r)
            self.assertIsNot('', _getLastDigit(r))

    def test_random_serial_without_digits(self):
        for i in range(20):
            r = random_serial(4, chars = string.ascii_uppercase, must_contain_digit = False)
            self.assertIsNotNone(r)
            last = _getLastDigit(r)
            logger.debug("have serial without digits: {}, last digit: {}".format(r, last))
            self.assertEqual('', last)

    def test_random_serial_with_digits(self):
        for i in range(20):
            r = random_serial(4, chars = string.ascii_uppercase, must_contain_digit = True)
            self.assertIsNotNone(r)
            logger.debug("have serial with digits: {}, last digit: {}".format(r, _getLastDigit(r)))

    def test_random_serial_with_digits2(self):
        for i in range(20):
            r = random_serial(4, chars = string.ascii_uppercase + string.digits, must_contain_digit = True)
            self.assertIsNotNone(r)
            self.assertIsNot('', _getLastDigit(r))
            logger.debug("have serial with digits 2: {}, last digit: {}".format(r, _getLastDigit(r)))

    def test_random_serial_maybe_with_digits(self):
        for i in range(20):
            r = random_serial(4, chars = string.ascii_uppercase + string.digits, must_contain_digit = False)
            self.assertIsNotNone(r)
            logger.debug("have serial maybe with digits: {}, last digit: {}".format(r, _getLastDigit(r)))

    def test_get_solutionfor3(self):
        serial = 'ABC579'
        # rule 3.1
        self.assertEqual(2, getSolution([WireColor.BLUE, WireColor.WHITE, WireColor.YELLOW], serial))
        self.assertEqual(2, getSolution([WireColor.BLACK, WireColor.BLUE, WireColor.BLUE], serial))
        # rule 3.2
        self.assertEqual(3, getSolution([WireColor.BLUE, WireColor.RED, WireColor.WHITE], serial))
        self.assertEqual(3, getSolution([WireColor.RED, WireColor.BLACK, WireColor.WHITE], serial))
        # rule 3.3
        self.assertEqual(3, getSolution([WireColor.BLUE, WireColor.RED, WireColor.BLUE], serial))
        self.assertEqual(2, getSolution([WireColor.BLUE, WireColor.BLUE, WireColor.RED], serial))
        # rule 3.4
        self.assertEqual(3, getSolution([WireColor.RED, WireColor.BLUE, WireColor.YELLOW], serial))
        self.assertEqual(3, getSolution([WireColor.BLACK, WireColor.RED, WireColor.BLUE], serial))

    def test_get_solutionfor4(self):
        # rule 4.1
        self.assertEqual(3, getSolution([WireColor.BLUE, WireColor.RED, WireColor.RED, WireColor.YELLOW], 'ABD789DEF'))
        self.assertEqual(2, getSolution([WireColor.RED, WireColor.RED, WireColor.BLUE, WireColor.YELLOW], 'ABD789DEF'))
        # rule 4.2
        self.assertEqual(1,
                         getSolution([WireColor.BLUE, WireColor.WHITE, WireColor.WHITE, WireColor.YELLOW], 'ABD789DEF'))
        self.assertEqual(1,
                         getSolution([WireColor.BLUE, WireColor.BLACK, WireColor.WHITE, WireColor.YELLOW], 'ABD246DEF'))
        # rule 4.3
        self.assertEqual(1, getSolution([WireColor.BLUE, WireColor.RED, WireColor.RED, WireColor.WHITE], 'A2B4C6DEF'))
        self.assertEqual(1,
                         getSolution([WireColor.BLUE, WireColor.RED, WireColor.BLACK, WireColor.YELLOW], 'ABD789DEF'))
        self.assertEqual(1, getSolution([WireColor.BLUE, WireColor.RED, WireColor.RED, WireColor.YELLOW], '246'))
        self.assertEqual(1, getSolution([WireColor.RED, WireColor.BLUE, WireColor.YELLOW, WireColor.WHITE], '999xy'))
        # rule 4.4
        self.assertEqual(4,
                         getSolution([WireColor.YELLOW, WireColor.BLACK, WireColor.RED, WireColor.YELLOW], 'ABD789DEF'))
        self.assertEqual(4, getSolution([WireColor.YELLOW, WireColor.RED, WireColor.YELLOW, WireColor.YELLOW], '111'))
        # rule 4.5
        self.assertEqual(2, getSolution([WireColor.YELLOW, WireColor.RED, WireColor.BLACK, WireColor.WHITE], '1367'))
        self.assertEqual(2, getSolution([WireColor.YELLOW, WireColor.RED, WireColor.BLACK, WireColor.BLACK], '1368'))


if __name__ == '__main__':
    main()
