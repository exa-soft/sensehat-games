from unittest import TestCase

from bombgame.core.exceptions import ArgumentError
from bombgame.simplewires import displayWires as dW


class Test(TestCase):

    def test_column_for_pos(self):
        self.assertEqual(1, dW._column_for_pos(3, 0))
        self.assertEqual(3, dW._column_for_pos(3, 1))
        self.assertEqual(5, dW._column_for_pos(3, 2))

        self.assertEqual(0, dW._column_for_pos(4, 0))
        self.assertEqual(2, dW._column_for_pos(4, 1))
        self.assertEqual(4, dW._column_for_pos(4, 2))
        self.assertEqual(6, dW._column_for_pos(4, 3))

    def test_pos_for_column(self):
        self.assertEqual(0, dW.pos_for_column(3, 1))
        self.assertEqual(1, dW.pos_for_column(3, 3))
        self.assertEqual(2, dW.pos_for_column(3, 5))

        self.assertEqual(0, dW.pos_for_column(4, 0))
        self.assertEqual(1, dW.pos_for_column(4, 2))
        self.assertEqual(2, dW.pos_for_column(4, 4))
        self.assertEqual(3, dW.pos_for_column(4, 6))

    # TODO expect ArgumentError: for other combination of arguments - test does not yet work
    def test_pos_for_column_errors(self):
        self.assertRaises(ArgumentError, dW.pos_for_column(3, 0))
        self.assertRaises(ArgumentError, dW.pos_for_column(3, 2))
        self.assertRaises(ArgumentError, dW.pos_for_column(3, 4))
        self.assertRaises(ArgumentError, dW.pos_for_column(3, 6))
        self.assertRaises(ArgumentError, dW.pos_for_column(3, 7))

        self.assertRaises(ArgumentError, dW.pos_for_column(4, 1))
        self.assertRaises(ArgumentError, dW.pos_for_column(4, 3))
        self.assertRaises(ArgumentError, dW.pos_for_column(4, 5))
        self.assertRaises(ArgumentError, dW.pos_for_column(4, 7))
